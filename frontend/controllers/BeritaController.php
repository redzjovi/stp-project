<?php

namespace frontend\controllers;

use common\models\Berita;

class BeritaController extends \yii\web\Controller
{
    public function actionDetail($id)
    {
    	$this->layout = 'home';
    	$data['berita'] = Berita::findOne($id);
        return $this->render('detail', $data);
    }
}
