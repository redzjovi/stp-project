<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class DashboardAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // 'css/site.css',
        'dashboard/css/bootstrap.min.css',
        'dashboard/css/font-awesome.min.css',
        'dashboard/css/animate.css',
        'dashboard/css/prettyPhoto.css',
        'dashboard/css/style.css',
    ];
    public $js = [
        'dashboard/js/jquery-2.1.1.min.js',
        'dashboard/js/bootstrap.min.js',
        'dashboard/js/jquery.prettyPhoto.js',
        'dashboard/js/jquery.isotope.min.js',
        'dashboard/js/wow.min.js',
        'dashboard/js/functions.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
