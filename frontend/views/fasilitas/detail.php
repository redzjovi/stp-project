<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Fasilitas';
$this->params['breadcrumbs'][] = $this->title;
?>
<img alt="<?= $fasilitas->nama_fasilitas ?>" src="<?= $fasilitas->getUploadGambar() ?>" width="100%" />
    <div class="bread_area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li><a href="/yii/frontend/web/site/index" title="Post">Home</a></li>
                        <li><a href="/yii/frontend/web/site/fasilitas"> Fasilitas</a></li>
                        <li class="active">Detail</li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>    
    <main class="site-main category-main">
        <div class="container">
            <div class="row">
                <aside class="sidebar col-sm-3">
                    <div class="widget">
                        <h4>FASILITAS STP</h4>
                        <ul>
                            <?php if ($leftfasilitases = \common\models\Fasilitas::find()->all()) : ?>
                                <?php foreach ($leftfasilitases as $i => $leftfasilitas) : ?>
                                    <li>
                                        <a href="<?= Url::to(['fasilitas/detail', 'id' => $leftfasilitas->id]) ?>" title="Post Title">
                                            <?= $leftfasilitas->nama_fasilitas ?>
                                        </a>
                                    </li>
                                <?php endforeach ?>
                            <?php endif ?>
                        </ul>
                    </div>
                </aside>
                 <section class="category-content col-sm-9">
                    <div class="widget">
                        <h2 class="category-title"><?= $fasilitas->nama_fasilitas ?></h2>
                        <P align="justify"><?= $fasilitas->isi_fasilitas ?></P>
                    </div>
                </section>
            </div>
        </div>
    </main>