<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Fasilitas';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <?php if ($fasilitass = \common\models\Fasilitas::find()->all()) : ?>
        <?php foreach ($fasilitass as $i => $fasilitas) : ?>
            <div class="item <?= $i == 0 ? 'active' : '' ?>">
                <img alt="<?= $fasilitas->nama_fasilitas ?>" src="<?= $fasilitas->getUploadGambar() ?>" width="100%" class="img-rounded"/>
            </div>
        <?php endforeach ?>
    <?php endif ?>
  </div>
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
    <div class="bread_area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li><a href="/yii/frontend/web" title="Post">Home</a></li>
                        <li class="active">Fasilitas</li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>    
    <main class="site-main category-main">
        <div class="container">
            <div class="row">
                <aside class="sidebar col-sm-3">
                    <div class="widget">
                        <h4>FASILITAS STP</h4>
                        <ul>
                            <?php if ($fasilitass = \common\models\Fasilitas::find()->all()) : ?>
                            <?php foreach ($fasilitass as $i => $fasilitas) : ?>
                            <li><a href="<?= Url::to(['fasilitas/detail', 'id' => $fasilitas->id]) ?>" title="Post Title"><?= $fasilitas->nama_fasilitas ?></a></li>
                           <?php endforeach ?>
                        <?php endif ?>
                        </ul>
                    </div>
                </aside>
                 <section class="category-content col-sm-9">
                    <div class="widget">
                        <h2 class="category-title">FASILITAS</h2>
                        <P align="justify">C-STP LIPI facilitates and synergizes elements of regional innovation system in nurturing and developing
                        knowledge based industry with the main objectives to enhance industry competitiveness and to improve productivity of the sosiety.</P>
                    </div>
                </section>
            </div>
        </div>
    </main>