<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Service';
$this->params['breadcrumbs'][] = $this->title;
?>
<img src="<?php echo Yii::getAlias('@web/images/home.png');?> " alt="Post" width="100%"/>
    <div class="bread_area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li><li><a href="/yii" title="Post">Home</a></li>
                        <li class="active">Service</li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>
            <main class="site-main page-main">
            <div class="container">
                <div class="row">
                    <section class="page col-sm-12">
                        <h2 class="page-title">FUNGSI CSTP</h2>
                    </section>
                </div>
                <?php if ($fungsis = \common\models\FungsiStp::find()->all()) : ?>
                    <?php foreach ($fungsis as $i => $fungsi) : ?>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="box-body">
                          <div class="alert alert-info alert-dismissible" >
                            <i class="icon fa fa-hand-o-right"></i> <?= $fungsi->fungsi ?>
                          </div>
                        </div>
                    </div>
                    <?php endforeach ?>
                <?php endif ?>
            </div>
        </main>
        <section class="services">
            <h2 class="section-title">SERVICES</h2>
            <!-- <p class="desc">Praesent faucibus ipsum at sodales blandit</p> -->
            <div class="container">
                <div class="row">
                    <?php if ($services = \common\models\Service::find()->all()) : ?>
                        <?php foreach ($services as $i => $service) : ?>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="media">
                                    <div class="media-left media-middle">
                                        <i class="fa fa-cogs"></i>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading"><?= $service->nama_service ?></h4>
                                        <p><?= $service->detail_service ?></p>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach ?>
                    <?php endif ?>
                </div>
            </div>
        </section>
