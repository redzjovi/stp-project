<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Berita';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="bread_area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li><a href="/yii" title="Post">Home</a></li>
                        <li class="active">Berita</li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>    
    <main class="site-main category-main">
        <div class="container">
            <div class="row">
                <section class="category-content col-sm-9">
                    <h2 class="category-title">BREAKING NEWS</h2>
                    <ul class="media-list">
                        <?php if ($beritas = \common\models\Berita::find()->all()) : ?>
                            <?php foreach ($beritas as $i => $berita) : ?>
                                <li class="media">
                                    <div class="media-left">
                                        <a href="#" title="Post">
                                            <img alt="<?= $berita->judul_berita ?>" src="<?= $berita->getUploadFoto() ?>" width="300px" />
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h3 class="media-heading"><a href="<?= Url::to(['berita/detail', 'id' => $berita->id]) ?>" title="Post Title"><?= $berita->judul_berita ?></a></h3>
                                        <p><?= \yii\helpers\StringHelper::truncate($berita->isi_berita, 200) ?></p>
                                        <aside class="meta category-meta">
                                            <div class="pull-left">
                                                <!-- <div class="arc-comment"><a href="#" title="Comment"><i class="fa fa-comments"></i> 0 Comment</a></div> -->
                                                <div class="arc-date"><?= $berita->tanggal_publikasi ?></div>
                                            </div>
                                            <div class="pull-right">
                                                <ul class="arc-share">
                                                    <li><a href="#" title="Post"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a href="#" title="Post"><i class="fa fa-twitter"></i></a></li>
                                                    <li><a href="#" title="Post"><i class="fa fa-linkedin"></i></a></li>
                                                    <li><a href="#" title="Post"><i class="fa fa-google-plus"></i></a></li>
                                                </ul>
                                            </div>
                                        </aside> 
                                    </div>
                                </li>
                            <?php endforeach ?>
                        <?php endif ?>
                    </ul>
                </section>
                <aside class="sidebar col-sm-3">
                    <div class="widget">
                        <h4>BERITA LAINNYA</h4>
                        <ul>
                            <?php if ($beritas = \common\models\Berita::find()->all()) : ?>
                            <?php foreach ($beritas as $i => $berita) : ?>
                            <li><a href="<?= Url::to(['berita/detail', 'id' => $berita->id]) ?>" title="Post Title"><?= $berita->judul_berita ?></a></li>
                           <?php endforeach ?>
                        <?php endif ?>
                        </ul>
                    </div>
                </aside>
            </div>
        </div>
    </main>