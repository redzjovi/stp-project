<div class="col-md-6 col-sm-6 col-xs-12">
    <div class="media">
        <div class="media-left media-middle">
            <img alt="<?= $model->nama_tenant ?>" src="<?= $model->getLogo() ?>" width="150px" />
        </div>
        <div class="media-left media-middle">
            <font color="black">
            <p><?= $model->nama_tenant ?></p></font>
            <p class="fa fa-map-signs"> <?= $model->alamat ?></p>
            <p class="fa fa-envelope-o"> <?= $model->email_tenant ?></p><br/>
            <p class="fa fa-phone"> <?= $model->no_hp ?></p>
        </div>
    </div>
</div>