<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Galery';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="bread_area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li><li><a href="/yii" title="Post">Home</a></li>
                        <li class="active">Galery STP</li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>
    <main class="site-main page-main">
        <div class="container">
				<div class="row">
				    <section class="page col-sm-12">
				        <h2 class="page-title">GALERY STP</h2>
				    </section>
				</div>
				<div class="row">
					<?php if ($galerys = \common\models\GaleryStp::find()->all()) : ?>
	                    <?php foreach ($galerys as $i => $galery) : ?>
	                    	<div class="col-sm-3">
								<div class="gallery">
								  <a target="_blank" href="/yii/frontend/web/images/slider.png">
								    <img alt="<?= $galery->nama ?>" src="<?= $galery->getFoto() ?>" width="300px" />
								  </a>
								  <div class="desc"><?= $galery->nama ?></div>
								</div>
							</div>
	                    <?php endforeach ?>
	                <?php endif ?>
            	</div>
				</div>
			</div>
		</main>
