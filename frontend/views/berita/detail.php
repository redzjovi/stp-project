<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Berita';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="bread_area">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ol class="breadcrumb">
                        <li><a href="/yii" title="Post">Home</a></li>
                        <li><a href="/yii/site/berita">Berita</a></li>
                        <li class="active">Detail</li>
                    </ol>                    
                </div>
            </div>
        </div>
    </div>    
    <main class="site-main category-main">
        <div class="container">
            <div class="row">
                <section class="category-content col-sm-9">
                    <h2 class="category-title">BERITA</h2>
                    <font color="black"><h3 style="text-transform: uppercase;"><?= $berita->judul_berita ?></h3></font>
                    <img alt="<?= $berita->judul_berita ?>" src="<?= $berita->getUploadFoto() ?>" width="600px" />
                    <br/>
                    <font color="blue"><h5><i class="fa fa-calendar"> Publikasi : <?= $berita->tanggal_publikasi ?></i></h5></font>
                    <p><?= $berita->isi_berita ?></p>
                    <p>Sumber Berita : <?= $berita->sumber_berita ?></p>
                </section>
                <aside class="sidebar col-sm-3">
                    <div class="widget">
                        <h4>BERITA LAINNYA</h4>
                        <ul>
                            <?php if ($beritaLainnya = \common\models\Berita::find()->all()) : ?>
                            <?php foreach ($beritaLainnya as $i => $berita) : ?>
                            <li><a href="<?= Url::to(['berita/detail', 'id' => $berita->id]) ?>" title="Post Title"><?= $berita->judul_berita ?></a></li>
                           <?php endforeach ?>
                        <?php endif ?>
                        </ul>
                    </div>
                </aside>
            </div>
        </div>
    </main>