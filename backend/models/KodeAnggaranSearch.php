<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\KodeAnggaran;

/**
 * KodeAnggaranSearch represents the model behind the search form of `common\models\KodeAnggaran`.
 */
class KodeAnggaranSearch extends KodeAnggaran
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'kode'], 'integer'],
            [['ket_anggaran'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KodeAnggaran::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'kode' => $this->kode,
        ]);

        $query->andFilterWhere(['like', 'ket_anggaran', $this->ket_anggaran]);

        return $dataProvider;
    }
}
