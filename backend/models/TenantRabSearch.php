<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TenantRab;

/**
 * TenantRabSearch represents the model behind the search form of `common\models\TenantRab`.
 */
class TenantRabSearch extends TenantRab
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_kegiatan'], 'integer'],
            [['jumlah'], 'number'],
            [['upload_rab'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TenantRab::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_kegiatan' => $this->id_kegiatan,
            'jumlah' => $this->jumlah,
        ]);

        $query->andFilterWhere(['like', 'upload_rab', $this->upload_rab]);

        return $dataProvider;
    }
}
