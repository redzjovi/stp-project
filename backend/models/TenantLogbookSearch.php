<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TenantLogbook;

/**
 * TenantLogbookSearch represents the model behind the search form of `common\models\TenantLogbook`.
 */
class TenantLogbookSearch extends TenantLogbook
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_kegiatan'], 'integer'],
            [['jenis_belanja', 'tanggal_pengajuan', 'tanggal_pencairan'], 'safe'],
            [['nominal'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TenantLogbook::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_kegiatan' => $this->id_kegiatan,
            'nominal' => $this->nominal,
            'tanggal_pengajuan' => $this->tanggal_pengajuan,
            'tanggal_pencairan' => $this->tanggal_pencairan,
        ]);

        $query->andFilterWhere(['like', 'jenis_belanja', $this->jenis_belanja]);

        return $dataProvider;
    }
}
