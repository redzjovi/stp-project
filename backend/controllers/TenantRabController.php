<?php

namespace backend\controllers;

use Yii;
use common\models\TenantRab;
use backend\models\TenantRabSearch;
use common\models\TenantKegiatan;
use backend\models\TenantKegiatanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * TenantRabController implements the CRUD actions for TenantRab model.
 */
class TenantRabController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TenantRab models.
     * @return mixed
     */
    public function actionRab()
    {
        $this->layout = 'admin';
        $searchModel = new TenantRabSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('rab', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndex()
    {
        $this->layout = 'admin';
        $searchModel = new TenantKegiatanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('Index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TenantRab model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->layout = 'admin';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TenantRab model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = 'admin';
        $model = new TenantRab();
        Yii::$app->params['uploadPath']= Yii::$app->basePath . '/web/images/rab/';

        if ($model->load(Yii::$app->request->post())) {
            $img = UploadedFile::getInstance($model, 'rab');
            $model->upload_rab = $img->name;
            $model->rab = $img;
            $model->save();
            $model->rab->saveAs(Yii::$app->params['uploadPath'] . $model->rab);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TenantRab model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->layout = 'admin';
        $model = $this->findModel($id);
        Yii::$app->params['uploadPath']= Yii::$app->basePath . '/web/images/rab/';

        if ($model->load(Yii::$app->request->post())) {
            $img = UploadedFile::getInstance($model, 'rab');
            $model->upload_rab = $img->name;
            $model->rab = $img;
            $model->save();
            $model->rab->saveAs(Yii::$app->params['uploadPath'] . $model->rab);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TenantRab model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->layout = 'admin';
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TenantRab model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TenantRab the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TenantRab::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
