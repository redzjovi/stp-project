<?php

namespace backend\controllers;

use Yii;
use common\models\DaftarTenant;
use backend\models\DaftarTenantSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;


/**
 * DaftarTenantController implements the CRUD actions for DaftarTenant model.
 */
class DaftarTenantController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DaftarTenant models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = 'admin';
        $searchModel = new DaftarTenantSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DaftarTenant model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $this->layout = 'admin';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DaftarTenant model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = 'admin';
        $model = new DaftarTenant();
        Yii::$app->params['uploadPath']= Yii::$app->basePath . '/web/images/logo-tenant/';

        if ($model->load(Yii::$app->request->post())) {
            $img = UploadedFile::getInstance($model, 'upload_logo');
            $model->logo = $img->name;
            $model->upload_logo = $img;
            $model->save();
            $model->upload_logo->saveAs(Yii::$app->params['uploadPath'] . $model->upload_logo);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing DaftarTenant model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->layout = 'admin';
        $model = $this->findModel($id);
        Yii::$app->params['uploadPath']= Yii::$app->basePath . '/web/images/logo-tenant/';
        if ($model->load(Yii::$app->request->post())) {

            $old = $model->logo;
                if($old!=""){
                    unlink(Yii::$app->basePath . '/web/images/logo-tenant/' . $old);
                }
                $img = UploadedFile::getInstance($model, 'upload_logo');
                $model->logo = $img->name;
                $model->upload_logo = $img;
                $model->save();
                $model->upload_logo->saveAs(Yii::$app->params['uploadPath'] . $model->upload_logo);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing DaftarTenant model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->layout = 'admin';
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DaftarTenant model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DaftarTenant the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DaftarTenant::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
