<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Berita */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Beritas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Daftar Berita
                    </h3>
                </div>
                <div class="panel-body" >
                    <div class="berita-view">

                        <h1><?= Html::encode($this->title) ?></h1>

                        <p>
                            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                            ]) ?>
                        </p>

                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'id',
                                'judul_berita',
                                'sumber_berita',
                                'isi_berita:raw',
                                'tanggal_publikasi',
                                   [
                                    'attribute' =>'upload_foto',
                                    'format' => 'raw',
                                    'value' => function ($row) {
                                        return $uploadFoto = Html::img(
                                            $row->getUploadFotoUrl(),
                                            ['style' => 'width:50px; heigth:20px;']
                                        );
                                    }
                                ],
                            ],
                        ]) ?>

                    </div>
                </div>
                <div class="panel-footer">
                    Panel footer
                </div>
            </div>
        </div>
    </div>
</div>

