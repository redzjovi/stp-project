<?php

use kartik\datecontrol\DateControl;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\Berita */
/* @var $form yii\widgets\ActiveForm */
?>
 <div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Daftar Berita
                    </h3>
                </div>
                <div class="panel-body" >
                    <div class="berita-form">

                        <?php $form = ActiveForm::begin(); ?>

                        <?= $form->field($model, 'judul_berita')->textInput(['maxlength' => true])->hint('Please enter your judul berita')?>

                        <?= $form->field($model, 'sumber_berita')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'isi_berita')->widget(CKEditor::className(), [
                            'options' => ['rows' => 6],
                            'preset' => 'advance'
                        ]) ?> 

                        <?= $form->field($model, 'tanggal_publikasi')->widget(DateControl::classname(), [
                            'type'=>DateControl::FORMAT_DATETIME])?>

                        <?= $form->field($model, 'gambar')->fileInput() ?>

                        <div class="form-group">
                            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>

                    </div>


                </div>
                <div class="panel-footer">
                    Panel footer
                </div>
            </div>
        </div>
    </div>
</div>



