<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $model common\models\Service */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Daftar Service
                </div>
                <div class="panel-body" >
					<div class="service-form">

					    <?php $form = ActiveForm::begin(); ?>

					    <?= $form->field($model, 'nama_service')->textarea(['rows' => 6]) ?>

					    <?= $form->field($model, 'detail_service')->widget(CKEditor::className(), [
                            'options' => ['rows' => 6],
                            'preset' => 'advance'
                        ]) ?>

					    <div class="form-group">
					        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
					    </div>

					    <?php ActiveForm::end(); ?>

					</div>
                </div>
                <div class="panel-footer">
                    Panel footer
                </div>
            </div>
        </div>
    </div>
</div>