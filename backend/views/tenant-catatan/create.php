<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TenantCatatan */

$this->title = 'Create Tenant Catatan';
$this->params['breadcrumbs'][] = ['label' => 'Tenant Catatans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenant-catatan-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
