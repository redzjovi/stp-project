<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TenantRabSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tenant Rabs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Daftar RAB Tenant
                    </h3>
                </div>
                <div class="panel-body" >
                    <div class="tenant-rab-index">

                        <!-- <h1><?= Html::encode($this->title) ?></h1> -->
                        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                        <p>
                            <?= Html::a('Create Tenant Rab', ['create', 'id_kegiatan' => \Yii::$app->request->get('TenantRabSearch')['id_kegiatan']], ['class' => 'btn btn-success']) ?>
                        </p>

                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                // 'id',
                                // 'id_kegiatan',
                                'jumlah',
                                [
                                    'attribute' =>'upload_rab',
                                    'format' => 'raw',
                                    'value' => function ($row) {
                                        return $uploadRab = Html::img(
                                            $row->getUploadRabUrl(),
                                            ['style' => 'width:50px; heigth:20px;']
                                        );
                                    }
                                ],

                                ['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]); ?>
                    </div>
                </div>
                <div class="panel-footer">
                    Panel footer
                </div>
            </div>
        </div>
    </div>
</div>