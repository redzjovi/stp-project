<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TenantRab */

$this->title = 'Create Tenant Rab';
$this->params['breadcrumbs'][] = ['label' => 'Tenant Rabs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenant-rab-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
