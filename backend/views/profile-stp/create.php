<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ProfileStp */

$this->title = 'Create Profile Stp';
$this->params['breadcrumbs'][] = ['label' => 'Profile Stps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profile-stp-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
