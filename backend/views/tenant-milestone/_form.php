<?php

use common\models\TenantKegiatan;
use common\models\TenantMilestone;
use kartik\datecontrol\DateControl;
use dosamigos\ckeditor\CKEditor;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model common\models\TenantMilestone */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Daftar Milestone Tenant
                    </h3>
                </div>
                <div class="panel-body" >
					<div class="tenant-milestone-form">

					    <?php $form = ActiveForm::begin(); ?>

					     <?php \Yii::$app->request->get('id_kegiatan') ? $model->id_kegiatan = \Yii::$app->request->get('id_kegiatan') : '' ?>
                        <?= $form->field($model, 'id_kegiatan')->hiddenInput()->label(false) ?>

                        <?php
                        // $form->field($model, 'id_kegiatan')->widget(Select2::classname(), [
                        //     'data' => ArrayHelper::map(TenantKegiatan::find()->isSelf()->orderBy('nama_kegiatan')->asArray()->all(), 'id', 'nama_kegiatan'),
                        //     'disabled' => true,
                        //     'options' => [
                        //         // 'disabled' => (! Yii::$app->user->can('Penelitian Pengajuan All')),
                        //         'placeholder' => 'Pilih Nama Nama Kegiatan ...',
                        //     ],
                        //     'pluginOptions' => [
                        //         'allowClear' => true,
                        //     ],
                        // ])
                        ?>

                        <?php
                        //  $form->field($model, 'bulan')->widget(Select2::classname(), [
                        //     'data' => TenantMilestone::lapBulan(),
                        //     'language' => 'de',
                        //     'options' => [
                        //          'placeholder' => 'Pilih Nama Bulan ...',
                        //         ],
                        //     'pluginOptions' => ['allowClear' => true],
                        // ]) 
                        ?>

                        <?= $form->field($model, 'bulan')->widget(DateControl::classname(), [
                            // 'ajaxConversion' => true, 
                            'displayFormat' => 'php:Y-M',
                            'saveFormat' => 'php:Y-m-d',
                            'widgetOptions' => [
                                'pluginOptions' => [
                                    'startView' => 1,
                                    'minViewMode' => 1,
                                ],
                            ],
                        ]) ?>

					    <?= $form->field($model, 'keterangan')->widget(CKEditor::className(), [
                            'options' => ['rows' => 6],
                            'preset' => 'advance'
                        ]) ?> 

					    <div class="form-group">
					        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
					    </div>

					    <?php ActiveForm::end(); ?>

					</div>
                </div>
                <div class="panel-footer">
                    Panel footer
                </div>
            </div>  
        </div>
    </div>
</div>

