<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FungsiStp */

$this->title = 'Create Fungsi Stp';
$this->params['breadcrumbs'][] = ['label' => 'Fungsi Stps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fungsi-stp-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
