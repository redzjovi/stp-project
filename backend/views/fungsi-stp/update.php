<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FungsiStp */

$this->title = 'Update Fungsi Stp';
$this->params['breadcrumbs'][] = ['label' => 'Fungsi Stps', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="fungsi-stp-update">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
