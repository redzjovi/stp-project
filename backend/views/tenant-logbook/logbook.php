<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TenantLogbookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tenant Logbooks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Daftar Log Book Tenant
                    </h3>
                </div>
                <div class="panel-body" >
                    <div class="tenant-logbook-index">

                        <!-- <h1><?= Html::encode($this->title) ?></h1> -->
                        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                        <p>
                            <?= Html::a('Create Tenant Logbook', ['create', 'id_kegiatan' => \Yii::$app->request->get('TenantLogbookSearch')['id_kegiatan']], ['class' => 'btn btn-success']) ?>
                        </p>

                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                // 'id',
                                // 'id_kegiatan',
                                'jenis_belanja',
                                'nominal',
                                'tanggal_pengajuan',
                                //'tanggal_pencairan',

                                ['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]); ?>
                    </div>
                </div>
                <div class="panel-footer">
                    Panel footer
                </div>
            </div>
        </div>
    </div>
</div>

