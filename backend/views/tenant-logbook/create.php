<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TenantLogbook */

$this->title = 'Create Tenant Logbook';
$this->params['breadcrumbs'][] = ['label' => 'Tenant Logbooks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenant-logbook-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
