<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TenantLogbookSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Daftar Log Book Tenant
                    </h3>
                </div>
                <div class="panel-body" >
                    <div class="tenant-logbook-search">

                        <?php $form = ActiveForm::begin([
                            'action' => ['index'],
                            'method' => 'get',
                        ]); ?>

                        <?= $form->field($model, 'id') ?>

                        <?= $form->field($model, 'id_kegiatan') ?>

                        <?= $form->field($model, 'jenis_belanja') ?>

                        <?= $form->field($model, 'nominal') ?>

                        <?= $form->field($model, 'tanggal_pengajuan') ?>

                        <?php // echo $form->field($model, 'tanggal_pencairan') ?>

                        <div class="form-group">
                            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>

                    </div>
                                    </div>
                <div class="panel-footer">
                    Panel footer
                </div>
            </div>
        </div>
    </div>
</div>

