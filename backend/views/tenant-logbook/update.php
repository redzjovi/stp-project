<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TenantLogbook */

$this->title = 'Update Tenant Logbook';
$this->params['breadcrumbs'][] = ['label' => 'Tenant Logbooks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tenant-logbook-update">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
