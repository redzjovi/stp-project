<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model common\models\PicStp */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Daftar PIC STP
                    </h3>
                </div>
                <div class="panel-body" >
                    <div class="pic-stp-form">

                        <?php $form = ActiveForm::begin(); ?>

                        <?= $form->field($model, 'nama_pic')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'jabatan')->textInput(['maxlength' => true]) ?>

                        <?php echo $form->field($model, 'tgl_lahir')->widget(DateControl::classname(), [
                            'type'=>DateControl::FORMAT_DATE,
                            'ajaxConversion'=>false,
                            'widgetOptions' => [
                                'pluginOptions' => [
                                    'autoclose' => true
                                ]
                            ]
                        ]); ?>

                        <?= $form->field($model, 'nip')->textInput(['maxlength' => true]) ?>

                        <?php 
                        $list = ['P'=>'Perempuan', 'L'=>'Laki-Laki'];
                        echo$form->field($model, 'jenis_kelamin')->radioList($list, ['inline'=>true]); ?>

                        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'id_user')->textInput() ?>

                        <div class="form-group">
                            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
                <div class="panel-footer">
                    Panel footer
                </div>
            </div>
        </div>
    </div>
</div>

