<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PicStp */

$this->title = 'Create Pic Stp';
$this->params['breadcrumbs'][] = ['label' => 'Pic Stps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pic-stp-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
