<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */

$this->title = 'Laporan Tenant';
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Daftar kegiatan Tenant
                    </h3>
                </div>
                <div class="panel-body" >
                    <div class="tenant-kegiatan-index">

                        <!-- <h1><?= Html::encode($this->title) ?></h1> -->
                        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                // 'id',
                                // 'nama_kegiatan',
                                'koordinator_peneliti',
                                'judul',
                                // 'tujuan:raw',
                                //'sasaran:raw',
                                // 'id_pic',

                                // ['class' => 'yii\grid\ActionColumn'],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{download}',
                                    'buttons' => [
                                        'download' => function ($url, $model, $key) {
                                            return Html::a(
                                                '<span class="glyphicon glyphicon-arrow-down"></span>',
                                                ['/default/report', 'id' => $model->id], 
                                                ['target' => '_blank', 'title' => 'Download']
                                            );
                                        },
                                    ],
                                ],
                            ],
                        ]); ?>
                    </div>
                </div>
                <div class="panel-footer">
                    Panel footer
                </div>
            </div>
        </div>
    </div>
</div>