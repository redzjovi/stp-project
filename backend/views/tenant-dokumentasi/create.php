<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TenantDokumentasi */

$this->title = 'Create Tenant Dokumentasi';
$this->params['breadcrumbs'][] = ['label' => 'Tenant Dokumentasis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenant-dokumentasi-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
