<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use common\models\PicStp;
/* @var $this yii\web\View */
/* @var $model common\models\TenantKegiatan */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Daftar kegiatan Tenant
                    </h3>
                </div>
                <div class="panel-body" >
                    <div class="tenant-kegiatan-form">

                        <?php $form = ActiveForm::begin(); ?>

                        <?php $model->id_user = \Yii::$app->user->id; ?>
                        <?= $form->field($model, 'id_user')->hiddenInput()->label(false) ?>

                        <?= $form->field($model, 'nama_kegiatan')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'koordinator_peneliti')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'judul')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'id_pic')->widget(Select2::classname(), [
                                    'data' => ArrayHelper::map(PicStp::find()->isSelf()->orderBy('nama_pic')->asArray()->all(), 'id', 'nama_pic'),
                                    'options' => [
                                        // 'disabled' => (! Yii::$app->user->can('Penelitian Pengajuan All')),
                                        'placeholder' => 'Pilih Nama PIC ...',
                                    ],
                                    'pluginOptions' => ['allowClear' => true],
                        ]) ?>

                        <?= $form->field($model, 'tujuan')->widget(CKEditor::className(), [
                            'options' => ['rows' => 6],
                            'preset' => 'advance'
                        ]) ?>

                        <?= $form->field($model, 'sasaran')->widget(CKEditor::className(), [
                            'options' => ['rows' => 6],
                            'preset' => 'advance'
                        ]) ?>

                        <div class="form-group">
                            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
                <div class="panel-footer">
                    Panel footer
                </div>
            </div>
        </div>
    </div>
</div>

