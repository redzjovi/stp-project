<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\KodeAnggaran */

$this->title = 'Create Kode Anggaran';
$this->params['breadcrumbs'][] = ['label' => 'Kode Anggarans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kode-anggaran-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
