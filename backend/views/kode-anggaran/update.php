<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\KodeAnggaran */

$this->title = 'Update Kode Anggaran';
$this->params['breadcrumbs'][] = ['label' => 'Kode Anggarans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kode-anggaran-update">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
