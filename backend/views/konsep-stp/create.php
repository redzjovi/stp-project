<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\KonsepStp */

$this->title = 'Create Konsep Stp';
$this->params['breadcrumbs'][] = ['label' => 'Konsep Stps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="konsep-stp-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
