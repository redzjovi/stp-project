<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\KonsepStp */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Konsep STP
                    </h3>
                </div>
                <div class="panel-body" >
                	<div class="konsep-stp-form">

					    <?php $form = ActiveForm::begin(); ?>

					     <?= $form->field($model, 'isi_konsep')->widget(CKEditor::className(), [
                            'options' => ['rows' => 6],
                            'preset' => 'advance'
                        ]) ?>

					    <div class="form-group">
					        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
					    </div>

					    <?php ActiveForm::end(); ?>

					</div>
                </div>
                <div class="panel-footer"> 
                    Panel footer
                </div>
            </div>
        </div>
    </div>
</div>



