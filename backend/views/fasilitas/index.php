<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\FasilitasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Fasilitas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Daftar Fasilitas
                    </h3>
                </div>
                <div class="panel-body" >
                    <div class="fasilitas-index">

                        <!-- <h1><?= Html::encode($this->title) ?></h1> -->
                        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                        <p>
                            <?= Html::a('Create Fasilitas', ['create'], ['class' => 'btn btn-success']) ?>
                        </p>

                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],

                                // 'id',
                                'nama_fasilitas',
                                'isi_fasilitas:raw',
                                [
                                    'attribute' =>'upload_gambar',
                                    'format' => 'raw',
                                    'value' => function ($row) {
                                        return $uploadGambar = Html::img(
                                            $row->getUploadGambarUrl(),
                                            ['style' => 'width:50px; heigth:20px;']
                                        );
                                    }
                                ],

                                ['class' => 'yii\grid\ActionColumn'],
                            ],
                        ]); ?>
                    </div>
                </div>
                <div class="panel-footer">
                    Panel footer
                </div>
            </div>
        </div>
    </div>
</div>

