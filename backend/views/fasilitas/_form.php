<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\Fasilitas */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        Daftar Fasilitas
                </div>
                <div class="panel-body" >
					<div class="fasilitas-form">

					    <?php $form = ActiveForm::begin(); ?>

					    <?= $form->field($model, 'nama_fasilitas')->textInput(['maxlength' => true]) ?>

					   <?= $form->field($model, 'isi_fasilitas')->widget(CKEditor::className(), [
                            'options' => ['rows' => 6],
                            'preset' => 'advance'
                        ]) ?>

					    <?= $form->field($model, 'gambar')->fileInput() ?>

					    <div class="form-group">
					        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
					    </div>

					    <?php ActiveForm::end(); ?>

					</div>
                </div>
                <div class="panel-footer">
                    Panel footer
                </div>
            </div>
        </div>
    </div>
</div>

