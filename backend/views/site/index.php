<?php
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Pendampingan';
?>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-info">
        <div class="panel-heading">
          <h3 class="panel-title">
              Form Pendampingan CSTP
          </h3>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-12">
                <h4 align="center"><b>PENDAMPINGAN KEGIATAN</b></h4>
                <h4 align="center"><b>CIBINONG SCIENCE AND TECHNOLOGY PARK</b></h4>
            </div>
          </div>
          <br/>
          <div class="row">
                  <div class="col-lg-4 col-xs-6">  
                    <div class="small-box" style="background-color: #AFEEEE">
                        <div class="inner" align="center">
                          <img src="<?php echo Yii::getAlias('@web/images/icon/i-1.png');?> " width='80px'/>
                          <p>Data Kegiatan Tenant</p>
                        </div>
                        <a href="<?php echo Url::to(['/tenant-kegiatan']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                  </div>
                  <div class="col-lg-4 col-xs-6">
                      <!-- small box -->
                      <div class="small-box" style="background-color: #AFEEEE">
                        <div class="inner" align="center">
                          <img src="<?php echo Yii::getAlias('@web/images/icon/i-2.png');?> " width='80px'/>
                          <p>RAB Kegiatan Tenant</p>
                        </div>
                        <a href="<?php echo Url::to(['/tenant-rab']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    <div class="col-lg-4 col-xs-6">
                      <!-- small box -->
                      <div class="small-box" style="background-color:#AFEEEE">
                        <div class="inner" align="center">
                          <img src="<?php echo Yii::getAlias('@web/images/icon/i-3.png');?> " width='80px'/>
                          <p> Milestone Kegiatan Tenant</p>
                        </div>
                        <a href="<?php echo Url::to(['/tenant-milestone']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                </div>
                <div class="row">
                  <div class="col-lg-4 col-xs-6">  
                    <div class="small-box" style="background-color: #AFEEEE">
                        <div class="inner" align="center">
                          <img src="<?php echo Yii::getAlias('@web/images/icon/i-4.png');?> " width='80px'/>
                          <p>Kolom Catatan Pendamping</p>
                        </div>
                        <a href="<?php echo Url::to(['/tenant-catatan']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                  </div>
                  <div class="col-lg-4 col-xs-6">
                      <!-- small box -->
                      <div class="small-box" style="background-color: #AFEEEE">
                        <div class="inner" align="center">
                          <img src="<?php echo Yii::getAlias('@web/images/icon/i-5.png');?> " width='80px'/>
                          <p>Log Book Anggaran</p>
                        </div>
                        <a href="<?php echo Url::to(['/tenant-logbook']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    <div class="col-lg-4 col-xs-6">
                      <!-- small box -->
                      <div class="small-box" style="background-color:#AFEEEE">
                        <div class="inner" align="center">
                          <img src="<?php echo Yii::getAlias('@web/images/icon/i-6.png');?> " width='80px'/>
                          <p>Dokumentasi Pendampingan</p>
                        </div>
                        <a href="<?php echo Url::to(['/tenant-dokumentasi']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                </div>
        <div class="panel-footer">
          Panel footer
        </div>
      </div>
    </div>
  </div>
</div>
              