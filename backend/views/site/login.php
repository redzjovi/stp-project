<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">    
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            
            <!-- Div Panel -->
            <div class="panel panel-success" >
                    
                    <!-- Panel Heading -->
                    <div class="panel-heading" style="background-color: #4169E1">
                        <div class="panel-title"><font color="white">Sign In</div></font>
                    </div><!-- End panel heading -->
                    
                    <!-- Panel body -->
                    <div class="panel-body" >
                        <div class="site-login" align="center">
                            <img src="<?php echo Yii::getAlias('@web/images/cstp.png');?> " alt="Post" width="150px"/>
                            <img src="<?php echo Yii::getAlias('@web/images/logo1.png');?> " alt="Post" width="80px"/>
                            <br/>
                            <h6 align="center"><p>Login untuk memulai session anda :</p></h6>

                            <div class="row" align="justify">
                                <div class="col-lg-12">
                                    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                                        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                                        <?= $form->field($model, 'password')->passwordInput() ?>

                                        <?= $form->field($model, 'rememberMe')->checkbox() ?>

                                        <div class="form-group">
                                            <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                                        </div>

                                    <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                        </div>
                    </div><!-- ENd panel body -->
            </div><!-- End panel group -->  

        </div><!-- End col div -->
    </div><!-- End container -->