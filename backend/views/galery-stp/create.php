<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\GaleryStp */

$this->title = 'Create Galery Stp';
$this->params['breadcrumbs'][] = ['label' => 'Galery Stps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="galery-stp-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
