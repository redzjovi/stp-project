<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AdminAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'admin2/bower_components/bootstrap/dist/css/bootstrap.min.css',
        'admin2/bower_components/font-awesome/css/font-awesome.min.css',
        'admin2/bower_components/Ionicons/css/ionicons.min.css',
        'admin2/dist/css/AdminLTE.min.css',
        'admin2/dist/css/skins/_all-skins.min.css',
        'admin2/bower_components/morris.js/morris.css',
        'admin2/bower_components/jvectormap/jquery-jvectormap.css',
        'admin2/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
        'admin2/bower_components/bootstrap-daterangepicker/daterangepicker.css',
        'admin2/plugins/timepicker/bootstrap-timepicker.min.css',
        'admin2/plugins/iCheck/all.css',
        'admin2/bower_components/select2/dist/css/select2.min.css',
        'admin2/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
        'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic',
    ];
    public $js = [
        // 'admin2/bower_components/jquery/dist/jquery.min.js',
        'admin2/bower_components/jquery-ui/jquery-ui.min.js',
        'admin2/button.js',
        'admin2/bower_components/bootstrap/dist/js/bootstrap.min.js',
        'admin2/bower_components/raphael/raphael.min.js',
        'admin2/bower_components/morris.js/morris.min.js',
        'admin2/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js',
        'admin2/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
        'admin2/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
        'admin2/bower_components/jquery-knob/dist/jquery.knob.min.js',
        'admin2/bower_components/moment/min/moment.min.js',
        'admin2/bower_components/select2/dist/js/select2.full.min.js',
        'admin2/plugins/input-mask/jquery.inputmask.js',
        'admin2/plugins/input-mask/jquery.inputmask.date.extensions.js',
        'admin2/plugins/input-mask/jquery.inputmask.extensions.js',
        'admin2/bower_components/moment/min/moment.min.js',
        'admin2/bower_components/bootstrap-daterangepicker/daterangepicker.js',
        'admin2/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
        'admin2/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js',
        'admin2/plugins/timepicker/bootstrap-timepicker.min.js',
        'admin2/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
        'admin2/bower_components/jquery-slimscroll/jquery.slimscroll.min.js',
        'admin2/plugins/iCheck/icheck.min.js',
        'admin2/bower_components/fastclick/lib/fastclick.js',
        'admin2/dist/js/adminlte.min.js',
        'admin2/dist/js/pages/dashboard.js',
        'admin2/dist/js/demo.js',
        'admin2/waktu.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
