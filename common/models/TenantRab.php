<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\web\UploadedFile;
/**
 * This is the model class for table "tenant_rab".
 *
 * @property int $id
 * @property int $id_kegiatan
 * @property double $jumlah
 * @property string $upload_rab
 *
 * @property TenantKegiatan $kegiatan
 */
class TenantRab extends \yii\db\ActiveRecord
{
    public $rab;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tenant_rab';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_kegiatan', 'jumlah', 'upload_rab'], 'required'],
            [['id_kegiatan'], 'integer'],
            [['jumlah'], 'number'],
            [['upload_rab'], 'string', 'max' => 200],
            [['id_kegiatan'], 'exist', 'skipOnError' => true, 'targetClass' => TenantKegiatan::className(), 'targetAttribute' => ['id_kegiatan' => 'id']],
            [['rab'], 'file', 'extensions' => 'jpg, jpeg, png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_kegiatan' => 'Id Kegiatan',
            'jumlah' => 'Jumlah',
            'upload_rab' => 'Upload Rab',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKegiatan()
    {
        return $this->hasOne(TenantKegiatan::className(), ['id' => 'id_kegiatan']);
    }

    public function getUploadRab()
    {
        return \Yii::$app->params['backendImagesUrl'].'/rab/'.$this->upload_rab;
    }

    public function getUploadRabUrl()
    {
        return $this->upload_rab ? Url::to('@web/images/rab/'.$this->upload_rab) : '';
    }
}
