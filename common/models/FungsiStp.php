<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "fungsi_stp".
 *
 * @property int $id
 * @property string $fungsi
 */
class FungsiStp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fungsi_stp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fungsi'], 'required'],
            [['fungsi'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fungsi' => 'Fungsi',
        ];
    }

    /**
     * @inheritdoc
     * @return FungsiStpQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FungsiStpQuery(get_called_class());
    }
}
