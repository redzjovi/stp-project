<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "kode_anggaran".
 *
 * @property int $id
 * @property int $kode
 * @property string $ket_anggaran
 */
class KodeAnggaran extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kode_anggaran';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode', 'ket_anggaran'], 'required'],
            [['kode'], 'integer'],
            [['ket_anggaran'], 'string', 'max' => 300],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kode' => 'Kode',
            'ket_anggaran' => 'Ket Anggaran',
        ];
    }
}
