<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "kegiatan".
 *
 * @property int $id
 * @property string $judul_kegiatan
 * @property string $isi_kegiatan
 */
class Kegiatan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kegiatan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['judul_kegiatan', 'isi_kegiatan'], 'required'],
            [['isi_kegiatan'], 'string'],
            [['judul_kegiatan'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'judul_kegiatan' => 'Judul Kegiatan',
            'isi_kegiatan' => 'Isi Kegiatan',
        ];
    }
}
