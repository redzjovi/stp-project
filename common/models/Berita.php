<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\web\UploadedFile;

/**
 * This is the model class for table "berita".
 *
 * @property int $id
 * @property string $judul_berita
 * @property string $sumber_berita
 * @property string $isi_berita
 * @property string $tanggal_publikasi
 * @property string $upload_foto
 */
class Berita extends \yii\db\ActiveRecord
{
    public $gambar;
    /** 
     * @inheritdoc 
     */
    public static function tableName()
    {
        return 'berita';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['judul_berita', 'sumber_berita', 'isi_berita', 'tanggal_publikasi', 'upload_foto'], 'required'],
            [['isi_berita'], 'string'],
            [['tanggal_publikasi'], 'safe'],
            [['judul_berita', 'sumber_berita'], 'string', 'max' => 200],
            [['upload_foto'], 'string', 'max' => 45],
            [['gambar'], 'file', 'extensions' => 'jpg, jpeg, png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'judul_berita' => 'Judul Berita',
            'sumber_berita' => 'Sumber Berita',
            'isi_berita' => 'Isi Berita',
            'tanggal_publikasi' => 'Tanggal Publikasi',
            'upload_foto' => 'Upload Foto',
        ];
    }

    public function getUploadFoto()
    {
        return \Yii::$app->params['backendImagesUrl'].'/image-berita/'.$this->upload_foto;
    }

    public function getUploadFotoUrl()
    {
        return $this->upload_foto ? Url::to('@web/images/image-berita/'.$this->upload_foto) : '';
    }
}
