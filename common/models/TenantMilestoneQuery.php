<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[TenantMilestone]].
 *
 * @see TenantMilestone
 */
class TenantMilestoneQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return TenantMilestone[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TenantMilestone|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
