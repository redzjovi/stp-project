<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tenant_kegiatan".
 *
 * @property int $id
 * @property int $id_user
 * @property string $nama_kegiatan
 * @property string $koordinator_peneliti
 * @property string $judul
 * @property string $tujuan
 * @property string $sasaran
 * @property int $id_pic
 *
 * @property TenantCatatan[] $tenantCatatans
 * @property TenantDokumentasi[] $tenantDokumentasis
 * @property PicStp $pic
 * @property User $user
 * @property TenantLogbook[] $tenantLogbooks
 * @property TenantMilestone[] $tenantMilestones
 * @property TenantRab[] $tenantRabs
 */
class TenantKegiatan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tenant_kegiatan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'nama_kegiatan', 'koordinator_peneliti', 'judul', 'tujuan', 'sasaran', 'id_pic'], 'required'],
            [['id_user', 'id_pic'], 'integer'],
            [['tujuan', 'sasaran'], 'string'],
            [['nama_kegiatan', 'koordinator_peneliti', 'judul'], 'string', 'max' => 300],
            [['id_pic'], 'exist', 'skipOnError' => true, 'targetClass' => PicStp::className(), 'targetAttribute' => ['id_pic' => 'id']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'nama_kegiatan' => 'Nama Kegiatan',
            'koordinator_peneliti' => 'Koordinator Peneliti',
            'judul' => 'Judul',
            'tujuan' => 'Tujuan',
            'sasaran' => 'Sasaran',
            'id_pic' => 'Id Pic',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantCatatans()
    {
        return $this->hasMany(TenantCatatan::className(), ['id_kegiatan' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantDokumentasis()
    {
        return $this->hasMany(TenantDokumentasi::className(), ['id_kegiatan' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPic()
    {
        return $this->hasOne(PicStp::className(), ['id' => 'id_pic']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantLogbooks()
    {
        return $this->hasMany(TenantLogbook::className(), ['id_kegiatan' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantMilestones()
    {
        return $this->hasMany(TenantMilestone::className(), ['id_kegiatan' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantRabs()
    {
        return $this->hasMany(TenantRab::className(), ['id_kegiatan' => 'id']);
    }

    /**
     * @inheritdoc
     * @return TenantKegiatanQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TenantKegiatanQuery(get_called_class());
    }
}
