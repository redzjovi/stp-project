<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tenant_logbook".
 *
 * @property int $id
 * @property int $id_kegiatan
 * @property string $jenis_belanja
 * @property double $nominal
 * @property string $tanggal_pengajuan
 * @property string $tanggal_pencairan
 *
 * @property TenantKegiatan $kegiatan
 */
class TenantLogbook extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tenant_logbook';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_kegiatan', 'jenis_belanja', 'nominal', 'tanggal_pengajuan', 'tanggal_pencairan'], 'required'],
            [['id_kegiatan'], 'integer'],
            [['nominal'], 'number'],
            [['tanggal_pengajuan', 'tanggal_pencairan'], 'safe'],
            [['jenis_belanja'], 'string', 'max' => 300],
            [['id_kegiatan'], 'exist', 'skipOnError' => true, 'targetClass' => TenantKegiatan::className(), 'targetAttribute' => ['id_kegiatan' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_kegiatan' => 'Id Kegiatan',
            'jenis_belanja' => 'Jenis Belanja',
            'nominal' => 'Nominal',
            'tanggal_pengajuan' => 'Tanggal Pengajuan',
            'tanggal_pencairan' => 'Tanggal Pencairan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKegiatan()
    {
        return $this->hasOne(TenantKegiatan::className(), ['id' => 'id_kegiatan']);
    }
}
