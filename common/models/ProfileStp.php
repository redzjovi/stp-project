<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "profile_stp".
 *
 * @property int $id
 * @property string $latar_belakang
 * @property string $visi
 * @property string $misi
 */
class ProfileStp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile_stp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['latar_belakang', 'visi', 'misi'], 'required'],
            [['latar_belakang', 'visi', 'misi'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'latar_belakang' => 'Latar Belakang',
            'visi' => 'Visi',
            'misi' => 'Misi',
        ];
    }
}
