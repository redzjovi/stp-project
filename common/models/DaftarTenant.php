<?php

namespace common\models;

use Yii;
use yii\helpers\Url;
use yii\web\UploadedFile;

/**
 * This is the model class for table "daftar_tenant".
 *
 * @property int $id
 * @property string $nama_tenant
 * @property string $email_tenant
 * @property string $alamat
 * @property string $no_hp
 * @property string $logo
 */
class DaftarTenant extends \yii\db\ActiveRecord
{
    public $upload_logo;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'daftar_tenant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_tenant', 'email_tenant', 'alamat', 'no_hp', 'logo'], 'required'],
            [['alamat'], 'string'],
            [['nama_tenant'], 'string', 'max' => 200],
            [['email_tenant'], 'string', 'max' => 300],
            [['no_hp', 'logo'], 'string', 'max' => 45],
            [['upload_logo'], 'file', 'extensions' => 'jpg, jpeg, png'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_tenant' => 'Nama Tenant',
            'email_tenant' => 'Email Tenant',
            'alamat' => 'Alamat',
            'no_hp' => 'No Hp',
            'logo' => 'Logo',
        ];
    }
    
    public function getLogo()
    {
        return \Yii::$app->params['backendImagesUrl'].'/logo-tenant/'.$this->logo;
    }

    public function getLogoUrl()
    {
        return $this->logo ? Url::to('@web/images/logo-tenant/'.$this->logo) : '';
    }
}
