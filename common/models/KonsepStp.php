<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "konsep_stp".
 *
 * @property int $id
 * @property string $isi_konsep
 */
class KonsepStp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'konsep_stp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['isi_konsep'], 'required'],
            [['isi_konsep'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'isi_konsep' => 'Isi Konsep',
        ];
    }
}
