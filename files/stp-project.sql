-- Adminer 4.6.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `auth_assignment`;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  KEY `auth_assignment_user_id_idx` (`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

TRUNCATE `auth_assignment`;
INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admin',	'1',	1524150252),
('user',	'10',	1526957637),
('user',	'2',	1524151046),
('user',	'3',	1524151056),
('user',	'4',	1526957553),
('user',	'5',	1526957566),
('user',	'6',	1526957575),
('user',	'7',	1526957591),
('user',	'8',	1526957620),
('user',	'9',	1526957629);

DROP TABLE IF EXISTS `auth_item`;
CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

TRUNCATE `auth_item`;
INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('/*',	2,	NULL,	NULL,	NULL,	1524150955,	1524150955),
('/admin/*',	2,	NULL,	NULL,	NULL,	1524150951,	1524150951),
('/admin/assignment/*',	2,	NULL,	NULL,	NULL,	1524150948,	1524150948),
('/admin/assignment/assign',	2,	NULL,	NULL,	NULL,	1524150948,	1524150948),
('/admin/assignment/index',	2,	NULL,	NULL,	NULL,	1524150948,	1524150948),
('/admin/assignment/revoke',	2,	NULL,	NULL,	NULL,	1524150948,	1524150948),
('/admin/assignment/view',	2,	NULL,	NULL,	NULL,	1524150948,	1524150948),
('/admin/default/*',	2,	NULL,	NULL,	NULL,	1524150949,	1524150949),
('/admin/default/index',	2,	NULL,	NULL,	NULL,	1524150948,	1524150948),
('/admin/menu/*',	2,	NULL,	NULL,	NULL,	1524150949,	1524150949),
('/admin/menu/create',	2,	NULL,	NULL,	NULL,	1524150949,	1524150949),
('/admin/menu/delete',	2,	NULL,	NULL,	NULL,	1524150949,	1524150949),
('/admin/menu/index',	2,	NULL,	NULL,	NULL,	1524150949,	1524150949),
('/admin/menu/update',	2,	NULL,	NULL,	NULL,	1524150949,	1524150949),
('/admin/menu/view',	2,	NULL,	NULL,	NULL,	1524150949,	1524150949),
('/admin/permission/*',	2,	NULL,	NULL,	NULL,	1524150949,	1524150949),
('/admin/permission/assign',	2,	NULL,	NULL,	NULL,	1524150949,	1524150949),
('/admin/permission/create',	2,	NULL,	NULL,	NULL,	1524150949,	1524150949),
('/admin/permission/delete',	2,	NULL,	NULL,	NULL,	1524150949,	1524150949),
('/admin/permission/index',	2,	NULL,	NULL,	NULL,	1524150949,	1524150949),
('/admin/permission/remove',	2,	NULL,	NULL,	NULL,	1524150949,	1524150949),
('/admin/permission/update',	2,	NULL,	NULL,	NULL,	1524150949,	1524150949),
('/admin/permission/view',	2,	NULL,	NULL,	NULL,	1524150949,	1524150949),
('/admin/role/*',	2,	NULL,	NULL,	NULL,	1524150950,	1524150950),
('/admin/role/assign',	2,	NULL,	NULL,	NULL,	1524150950,	1524150950),
('/admin/role/create',	2,	NULL,	NULL,	NULL,	1524150949,	1524150949),
('/admin/role/delete',	2,	NULL,	NULL,	NULL,	1524150950,	1524150950),
('/admin/role/index',	2,	NULL,	NULL,	NULL,	1524150949,	1524150949),
('/admin/role/remove',	2,	NULL,	NULL,	NULL,	1524150950,	1524150950),
('/admin/role/update',	2,	NULL,	NULL,	NULL,	1524150949,	1524150949),
('/admin/role/view',	2,	NULL,	NULL,	NULL,	1524150949,	1524150949),
('/admin/route/*',	2,	NULL,	NULL,	NULL,	1524150950,	1524150950),
('/admin/route/assign',	2,	NULL,	NULL,	NULL,	1524150950,	1524150950),
('/admin/route/create',	2,	NULL,	NULL,	NULL,	1524150950,	1524150950),
('/admin/route/index',	2,	NULL,	NULL,	NULL,	1524150950,	1524150950),
('/admin/route/refresh',	2,	NULL,	NULL,	NULL,	1524150950,	1524150950),
('/admin/route/remove',	2,	NULL,	NULL,	NULL,	1524150950,	1524150950),
('/admin/rule/*',	2,	NULL,	NULL,	NULL,	1524150950,	1524150950),
('/admin/rule/create',	2,	NULL,	NULL,	NULL,	1524150950,	1524150950),
('/admin/rule/delete',	2,	NULL,	NULL,	NULL,	1524150950,	1524150950),
('/admin/rule/index',	2,	NULL,	NULL,	NULL,	1524150950,	1524150950),
('/admin/rule/update',	2,	NULL,	NULL,	NULL,	1524150950,	1524150950),
('/admin/rule/view',	2,	NULL,	NULL,	NULL,	1524150950,	1524150950),
('/admin/user/*',	2,	NULL,	NULL,	NULL,	1524150951,	1524150951),
('/admin/user/activate',	2,	NULL,	NULL,	NULL,	1524150951,	1524150951),
('/admin/user/change-password',	2,	NULL,	NULL,	NULL,	1524150951,	1524150951),
('/admin/user/delete',	2,	NULL,	NULL,	NULL,	1524150950,	1524150950),
('/admin/user/index',	2,	NULL,	NULL,	NULL,	1524150950,	1524150950),
('/admin/user/login',	2,	NULL,	NULL,	NULL,	1524150951,	1524150951),
('/admin/user/logout',	2,	NULL,	NULL,	NULL,	1524150951,	1524150951),
('/admin/user/request-password-reset',	2,	NULL,	NULL,	NULL,	1524150951,	1524150951),
('/admin/user/reset-password',	2,	NULL,	NULL,	NULL,	1524150951,	1524150951),
('/admin/user/signup',	2,	NULL,	NULL,	NULL,	1524150951,	1524150951),
('/admin/user/view',	2,	NULL,	NULL,	NULL,	1524150950,	1524150950),
('/berita/*',	2,	NULL,	NULL,	NULL,	1524150785,	1524150785),
('/berita/create',	2,	NULL,	NULL,	NULL,	1524150790,	1524150790),
('/berita/delete',	2,	NULL,	NULL,	NULL,	1524150790,	1524150790),
('/berita/index',	2,	NULL,	NULL,	NULL,	1524150790,	1524150790),
('/berita/update',	2,	NULL,	NULL,	NULL,	1524150790,	1524150790),
('/berita/view',	2,	NULL,	NULL,	NULL,	1524150790,	1524150790),
('/daftar-tenant/*',	2,	NULL,	NULL,	NULL,	1524150952,	1524150952),
('/daftar-tenant/create',	2,	NULL,	NULL,	NULL,	1524150952,	1524150952),
('/daftar-tenant/delete',	2,	NULL,	NULL,	NULL,	1524150952,	1524150952),
('/daftar-tenant/index',	2,	NULL,	NULL,	NULL,	1524150952,	1524150952),
('/daftar-tenant/update',	2,	NULL,	NULL,	NULL,	1524150952,	1524150952),
('/daftar-tenant/view',	2,	NULL,	NULL,	NULL,	1524150952,	1524150952),
('/datecontrol/*',	2,	NULL,	NULL,	NULL,	1524150948,	1524150948),
('/datecontrol/parse/*',	2,	NULL,	NULL,	NULL,	1524150948,	1524150948),
('/datecontrol/parse/convert',	2,	NULL,	NULL,	NULL,	1524150948,	1524150948),
('/debug/*',	2,	NULL,	NULL,	NULL,	1524150951,	1524150951),
('/debug/default/*',	2,	NULL,	NULL,	NULL,	1524150951,	1524150951),
('/debug/default/db-explain',	2,	NULL,	NULL,	NULL,	1524150951,	1524150951),
('/debug/default/download-mail',	2,	NULL,	NULL,	NULL,	1524150951,	1524150951),
('/debug/default/index',	2,	NULL,	NULL,	NULL,	1524150951,	1524150951),
('/debug/default/toolbar',	2,	NULL,	NULL,	NULL,	1524150951,	1524150951),
('/debug/default/view',	2,	NULL,	NULL,	NULL,	1524150951,	1524150951),
('/debug/user/*',	2,	NULL,	NULL,	NULL,	1524150951,	1524150951),
('/debug/user/reset-identity',	2,	NULL,	NULL,	NULL,	1524150951,	1524150951),
('/debug/user/set-identity',	2,	NULL,	NULL,	NULL,	1524150951,	1524150951),
('/default/*',	2,	NULL,	NULL,	NULL,	1526365108,	1526365108),
('/default/index',	2,	NULL,	NULL,	NULL,	1526365108,	1526365108),
('/default/report',	2,	NULL,	NULL,	NULL,	1526444242,	1526444242),
('/fasilitas/*',	2,	NULL,	NULL,	NULL,	1524150952,	1524150952),
('/fasilitas/create',	2,	NULL,	NULL,	NULL,	1524150952,	1524150952),
('/fasilitas/delete',	2,	NULL,	NULL,	NULL,	1524150952,	1524150952),
('/fasilitas/index',	2,	NULL,	NULL,	NULL,	1524150952,	1524150952),
('/fasilitas/update',	2,	NULL,	NULL,	NULL,	1524150952,	1524150952),
('/fasilitas/view',	2,	NULL,	NULL,	NULL,	1524150952,	1524150952),
('/fungsi-stp/*',	2,	NULL,	NULL,	NULL,	1526444242,	1526444242),
('/fungsi-stp/create',	2,	NULL,	NULL,	NULL,	1526444242,	1526444242),
('/fungsi-stp/delete',	2,	NULL,	NULL,	NULL,	1526444242,	1526444242),
('/fungsi-stp/index',	2,	NULL,	NULL,	NULL,	1526444242,	1526444242),
('/fungsi-stp/update',	2,	NULL,	NULL,	NULL,	1526444242,	1526444242),
('/fungsi-stp/view',	2,	NULL,	NULL,	NULL,	1526444242,	1526444242),
('/galery-stp/*',	2,	NULL,	NULL,	NULL,	1524150952,	1524150952),
('/galery-stp/create',	2,	NULL,	NULL,	NULL,	1524150952,	1524150952),
('/galery-stp/delete',	2,	NULL,	NULL,	NULL,	1524150952,	1524150952),
('/galery-stp/index',	2,	NULL,	NULL,	NULL,	1524150952,	1524150952),
('/galery-stp/update',	2,	NULL,	NULL,	NULL,	1524150952,	1524150952),
('/galery-stp/view',	2,	NULL,	NULL,	NULL,	1524150952,	1524150952),
('/gii/*',	2,	NULL,	NULL,	NULL,	1524150952,	1524150952),
('/gii/default/*',	2,	NULL,	NULL,	NULL,	1524150952,	1524150952),
('/gii/default/action',	2,	NULL,	NULL,	NULL,	1524150951,	1524150951),
('/gii/default/diff',	2,	NULL,	NULL,	NULL,	1524150951,	1524150951),
('/gii/default/index',	2,	NULL,	NULL,	NULL,	1524150951,	1524150951),
('/gii/default/preview',	2,	NULL,	NULL,	NULL,	1524150951,	1524150951),
('/gii/default/view',	2,	NULL,	NULL,	NULL,	1524150951,	1524150951),
('/kegiatan/*',	2,	NULL,	NULL,	NULL,	1524150952,	1524150952),
('/kegiatan/create',	2,	NULL,	NULL,	NULL,	1524150952,	1524150952),
('/kegiatan/delete',	2,	NULL,	NULL,	NULL,	1524150952,	1524150952),
('/kegiatan/index',	2,	NULL,	NULL,	NULL,	1524150952,	1524150952),
('/kegiatan/update',	2,	NULL,	NULL,	NULL,	1524150952,	1524150952),
('/kegiatan/view',	2,	NULL,	NULL,	NULL,	1524150952,	1524150952),
('/kode-anggaran/*',	2,	NULL,	NULL,	NULL,	1524150953,	1524150953),
('/kode-anggaran/create',	2,	NULL,	NULL,	NULL,	1524150953,	1524150953),
('/kode-anggaran/delete',	2,	NULL,	NULL,	NULL,	1524150953,	1524150953),
('/kode-anggaran/index',	2,	NULL,	NULL,	NULL,	1524150952,	1524150952),
('/kode-anggaran/update',	2,	NULL,	NULL,	NULL,	1524150953,	1524150953),
('/kode-anggaran/view',	2,	NULL,	NULL,	NULL,	1524150952,	1524150952),
('/pic-stp/*',	2,	NULL,	NULL,	NULL,	1524150167,	1524150167),
('/pic-stp/create',	2,	NULL,	NULL,	NULL,	1524150167,	1524150167),
('/pic-stp/delete',	2,	NULL,	NULL,	NULL,	1524150167,	1524150167),
('/pic-stp/index',	2,	NULL,	NULL,	NULL,	1524150167,	1524150167),
('/pic-stp/update',	2,	NULL,	NULL,	NULL,	1524150167,	1524150167),
('/pic-stp/view',	2,	NULL,	NULL,	NULL,	1524150167,	1524150167),
('/profile-stp/*',	2,	NULL,	NULL,	NULL,	1524150953,	1524150953),
('/profile-stp/create',	2,	NULL,	NULL,	NULL,	1524150953,	1524150953),
('/profile-stp/delete',	2,	NULL,	NULL,	NULL,	1524150953,	1524150953),
('/profile-stp/index',	2,	NULL,	NULL,	NULL,	1524150953,	1524150953),
('/profile-stp/update',	2,	NULL,	NULL,	NULL,	1524150953,	1524150953),
('/profile-stp/view',	2,	NULL,	NULL,	NULL,	1524150953,	1524150953),
('/service/*',	2,	NULL,	NULL,	NULL,	1524150953,	1524150953),
('/service/create',	2,	NULL,	NULL,	NULL,	1524150953,	1524150953),
('/service/delete',	2,	NULL,	NULL,	NULL,	1524150953,	1524150953),
('/service/index',	2,	NULL,	NULL,	NULL,	1524150953,	1524150953),
('/service/update',	2,	NULL,	NULL,	NULL,	1524150953,	1524150953),
('/service/view',	2,	NULL,	NULL,	NULL,	1524150953,	1524150953),
('/site/*',	2,	NULL,	NULL,	NULL,	1524150953,	1524150953),
('/site/daftarhasil',	2,	NULL,	NULL,	NULL,	1526264516,	1526264516),
('/site/error',	2,	NULL,	NULL,	NULL,	1524150953,	1524150953),
('/site/index',	2,	NULL,	NULL,	NULL,	1524150953,	1524150953),
('/site/login',	2,	NULL,	NULL,	NULL,	1524150953,	1524150953),
('/site/logout',	2,	NULL,	NULL,	NULL,	1524150953,	1524150953),
('/site/tes',	2,	NULL,	NULL,	NULL,	1524150953,	1524150953),
('/tenant-catatan/*',	2,	NULL,	NULL,	NULL,	1524150954,	1524150954),
('/tenant-catatan/catatan',	2,	NULL,	NULL,	NULL,	1526028166,	1526028166),
('/tenant-catatan/create',	2,	NULL,	NULL,	NULL,	1524150954,	1524150954),
('/tenant-catatan/delete',	2,	NULL,	NULL,	NULL,	1524150954,	1524150954),
('/tenant-catatan/index',	2,	NULL,	NULL,	NULL,	1524150953,	1524150953),
('/tenant-catatan/update',	2,	NULL,	NULL,	NULL,	1524150954,	1524150954),
('/tenant-catatan/view',	2,	NULL,	NULL,	NULL,	1524150953,	1524150953),
('/tenant-dokumentasi/*',	2,	NULL,	NULL,	NULL,	1524150954,	1524150954),
('/tenant-dokumentasi/create',	2,	NULL,	NULL,	NULL,	1524150954,	1524150954),
('/tenant-dokumentasi/delete',	2,	NULL,	NULL,	NULL,	1524150954,	1524150954),
('/tenant-dokumentasi/dokumentasi',	2,	NULL,	NULL,	NULL,	1526028166,	1526028166),
('/tenant-dokumentasi/index',	2,	NULL,	NULL,	NULL,	1524150954,	1524150954),
('/tenant-dokumentasi/update',	2,	NULL,	NULL,	NULL,	1524150954,	1524150954),
('/tenant-dokumentasi/view',	2,	NULL,	NULL,	NULL,	1524150954,	1524150954),
('/tenant-kegiatan/*',	2,	NULL,	NULL,	NULL,	1524150954,	1524150954),
('/tenant-kegiatan/create',	2,	NULL,	NULL,	NULL,	1524150954,	1524150954),
('/tenant-kegiatan/delete',	2,	NULL,	NULL,	NULL,	1524150954,	1524150954),
('/tenant-kegiatan/index',	2,	NULL,	NULL,	NULL,	1524150954,	1524150954),
('/tenant-kegiatan/update',	2,	NULL,	NULL,	NULL,	1524150954,	1524150954),
('/tenant-kegiatan/view',	2,	NULL,	NULL,	NULL,	1524150954,	1524150954),
('/tenant-logbook/*',	2,	NULL,	NULL,	NULL,	1524150954,	1524150954),
('/tenant-logbook/create',	2,	NULL,	NULL,	NULL,	1524150954,	1524150954),
('/tenant-logbook/delete',	2,	NULL,	NULL,	NULL,	1524150954,	1524150954),
('/tenant-logbook/index',	2,	NULL,	NULL,	NULL,	1524150954,	1524150954),
('/tenant-logbook/logbook',	2,	NULL,	NULL,	NULL,	1526028167,	1526028167),
('/tenant-logbook/update',	2,	NULL,	NULL,	NULL,	1524150954,	1524150954),
('/tenant-logbook/view',	2,	NULL,	NULL,	NULL,	1524150954,	1524150954),
('/tenant-milestone/*',	2,	NULL,	NULL,	NULL,	1524150955,	1524150955),
('/tenant-milestone/create',	2,	NULL,	NULL,	NULL,	1524150955,	1524150955),
('/tenant-milestone/delete',	2,	NULL,	NULL,	NULL,	1524150955,	1524150955),
('/tenant-milestone/index',	2,	NULL,	NULL,	NULL,	1524150954,	1524150954),
('/tenant-milestone/milestone',	2,	NULL,	NULL,	NULL,	1526028167,	1526028167),
('/tenant-milestone/update',	2,	NULL,	NULL,	NULL,	1524150955,	1524150955),
('/tenant-milestone/view',	2,	NULL,	NULL,	NULL,	1524150954,	1524150954),
('/tenant-rab/*',	2,	NULL,	NULL,	NULL,	1524150955,	1524150955),
('/tenant-rab/create',	2,	NULL,	NULL,	NULL,	1524150955,	1524150955),
('/tenant-rab/delete',	2,	NULL,	NULL,	NULL,	1524150955,	1524150955),
('/tenant-rab/index',	2,	NULL,	NULL,	NULL,	1524150955,	1524150955),
('/tenant-rab/rab',	2,	NULL,	NULL,	NULL,	1526028167,	1526028167),
('/tenant-rab/update',	2,	NULL,	NULL,	NULL,	1524150955,	1524150955),
('/tenant-rab/view',	2,	NULL,	NULL,	NULL,	1524150955,	1524150955),
('admin',	1,	NULL,	NULL,	NULL,	1524150235,	1524150235),
('backend_pic_stp_all',	2,	NULL,	NULL,	NULL,	1524152437,	1524152437),
('backend_tenant_kegiatan_all',	2,	NULL,	NULL,	NULL,	1524148980,	1524148980),
('Left Menu Admin',	2,	NULL,	NULL,	NULL,	1525965987,	1525965987),
('Left Menu Berita',	2,	NULL,	NULL,	NULL,	1526028006,	1526028006),
('Left Menu Fasilitas',	2,	NULL,	NULL,	NULL,	1526027994,	1526027994),
('Left Menu Galery',	2,	NULL,	NULL,	NULL,	1526027981,	1526027981),
('Left Menu Kode',	2,	NULL,	NULL,	NULL,	1526028020,	1526028020),
('Left Menu RBAC',	2,	NULL,	NULL,	NULL,	1526026914,	1526026914),
('Left Menu Service',	2,	NULL,	NULL,	NULL,	1526027967,	1526027967),
('user',	1,	NULL,	NULL,	NULL,	1524151011,	1524151011);

DROP TABLE IF EXISTS `auth_item_child`;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

TRUNCATE `auth_item_child`;
INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('admin',	'/*'),
('admin',	'/admin/*'),
('admin',	'/admin/assignment/*'),
('admin',	'/admin/assignment/assign'),
('admin',	'/admin/assignment/index'),
('admin',	'/admin/assignment/revoke'),
('admin',	'/admin/assignment/view'),
('admin',	'/admin/default/*'),
('admin',	'/admin/default/index'),
('admin',	'/admin/menu/*'),
('admin',	'/admin/menu/create'),
('admin',	'/admin/menu/delete'),
('admin',	'/admin/menu/index'),
('admin',	'/admin/menu/update'),
('admin',	'/admin/menu/view'),
('admin',	'/admin/permission/*'),
('admin',	'/admin/permission/assign'),
('admin',	'/admin/permission/create'),
('admin',	'/admin/permission/delete'),
('admin',	'/admin/permission/index'),
('admin',	'/admin/permission/remove'),
('admin',	'/admin/permission/update'),
('admin',	'/admin/permission/view'),
('admin',	'/admin/role/*'),
('admin',	'/admin/role/assign'),
('admin',	'/admin/role/create'),
('admin',	'/admin/role/delete'),
('admin',	'/admin/role/index'),
('admin',	'/admin/role/remove'),
('admin',	'/admin/role/update'),
('admin',	'/admin/role/view'),
('admin',	'/admin/route/*'),
('admin',	'/admin/route/assign'),
('admin',	'/admin/route/create'),
('admin',	'/admin/route/index'),
('admin',	'/admin/route/refresh'),
('admin',	'/admin/route/remove'),
('admin',	'/admin/rule/*'),
('admin',	'/admin/rule/create'),
('admin',	'/admin/rule/delete'),
('admin',	'/admin/rule/index'),
('admin',	'/admin/rule/update'),
('admin',	'/admin/rule/view'),
('admin',	'/admin/user/*'),
('admin',	'/admin/user/activate'),
('admin',	'/admin/user/change-password'),
('admin',	'/admin/user/delete'),
('admin',	'/admin/user/index'),
('admin',	'/admin/user/login'),
('admin',	'/admin/user/logout'),
('admin',	'/admin/user/request-password-reset'),
('admin',	'/admin/user/reset-password'),
('admin',	'/admin/user/signup'),
('admin',	'/admin/user/view'),
('admin',	'/berita/*'),
('admin',	'/berita/create'),
('admin',	'/berita/delete'),
('admin',	'/berita/index'),
('admin',	'/berita/update'),
('admin',	'/berita/view'),
('admin',	'/daftar-tenant/*'),
('user',	'/daftar-tenant/*'),
('admin',	'/daftar-tenant/create'),
('user',	'/daftar-tenant/create'),
('admin',	'/daftar-tenant/delete'),
('user',	'/daftar-tenant/delete'),
('admin',	'/daftar-tenant/index'),
('user',	'/daftar-tenant/index'),
('admin',	'/daftar-tenant/update'),
('user',	'/daftar-tenant/update'),
('admin',	'/daftar-tenant/view'),
('user',	'/daftar-tenant/view'),
('admin',	'/datecontrol/*'),
('admin',	'/datecontrol/parse/*'),
('admin',	'/datecontrol/parse/convert'),
('admin',	'/debug/*'),
('user',	'/debug/*'),
('admin',	'/debug/default/*'),
('admin',	'/debug/default/db-explain'),
('admin',	'/debug/default/download-mail'),
('admin',	'/debug/default/index'),
('admin',	'/debug/default/toolbar'),
('admin',	'/debug/default/view'),
('admin',	'/debug/user/*'),
('admin',	'/debug/user/reset-identity'),
('admin',	'/debug/user/set-identity'),
('admin',	'/default/*'),
('user',	'/default/*'),
('admin',	'/default/index'),
('user',	'/default/index'),
('admin',	'/default/report'),
('admin',	'/fasilitas/*'),
('admin',	'/fasilitas/create'),
('admin',	'/fasilitas/delete'),
('admin',	'/fasilitas/index'),
('admin',	'/fasilitas/update'),
('admin',	'/fasilitas/view'),
('admin',	'/fungsi-stp/*'),
('admin',	'/fungsi-stp/create'),
('admin',	'/fungsi-stp/delete'),
('admin',	'/fungsi-stp/index'),
('admin',	'/fungsi-stp/update'),
('admin',	'/fungsi-stp/view'),
('admin',	'/galery-stp/*'),
('admin',	'/galery-stp/create'),
('admin',	'/galery-stp/delete'),
('admin',	'/galery-stp/index'),
('admin',	'/galery-stp/update'),
('admin',	'/galery-stp/view'),
('admin',	'/gii/*'),
('admin',	'/gii/default/*'),
('admin',	'/gii/default/action'),
('admin',	'/gii/default/diff'),
('admin',	'/gii/default/index'),
('admin',	'/gii/default/preview'),
('admin',	'/gii/default/view'),
('admin',	'/kegiatan/*'),
('admin',	'/kegiatan/create'),
('admin',	'/kegiatan/delete'),
('admin',	'/kegiatan/index'),
('admin',	'/kegiatan/update'),
('admin',	'/kegiatan/view'),
('admin',	'/kode-anggaran/*'),
('admin',	'/kode-anggaran/create'),
('admin',	'/kode-anggaran/delete'),
('admin',	'/kode-anggaran/index'),
('admin',	'/kode-anggaran/update'),
('admin',	'/kode-anggaran/view'),
('admin',	'/pic-stp/*'),
('admin',	'/pic-stp/create'),
('admin',	'/pic-stp/delete'),
('admin',	'/pic-stp/index'),
('admin',	'/pic-stp/update'),
('admin',	'/pic-stp/view'),
('admin',	'/profile-stp/*'),
('admin',	'/profile-stp/create'),
('admin',	'/profile-stp/delete'),
('admin',	'/profile-stp/index'),
('admin',	'/profile-stp/update'),
('admin',	'/profile-stp/view'),
('admin',	'/service/*'),
('admin',	'/service/create'),
('admin',	'/service/delete'),
('admin',	'/service/index'),
('admin',	'/service/update'),
('admin',	'/service/view'),
('admin',	'/site/*'),
('admin',	'/site/daftarhasil'),
('user',	'/site/daftarhasil'),
('admin',	'/site/error'),
('admin',	'/site/index'),
('user',	'/site/index'),
('admin',	'/site/login'),
('user',	'/site/login'),
('admin',	'/site/logout'),
('user',	'/site/logout'),
('admin',	'/site/tes'),
('admin',	'/tenant-catatan/*'),
('user',	'/tenant-catatan/*'),
('admin',	'/tenant-catatan/catatan'),
('admin',	'/tenant-catatan/create'),
('user',	'/tenant-catatan/create'),
('admin',	'/tenant-catatan/delete'),
('user',	'/tenant-catatan/delete'),
('admin',	'/tenant-catatan/index'),
('user',	'/tenant-catatan/index'),
('admin',	'/tenant-catatan/update'),
('user',	'/tenant-catatan/update'),
('admin',	'/tenant-catatan/view'),
('user',	'/tenant-catatan/view'),
('admin',	'/tenant-dokumentasi/*'),
('user',	'/tenant-dokumentasi/*'),
('admin',	'/tenant-dokumentasi/create'),
('user',	'/tenant-dokumentasi/create'),
('admin',	'/tenant-dokumentasi/delete'),
('user',	'/tenant-dokumentasi/delete'),
('admin',	'/tenant-dokumentasi/dokumentasi'),
('admin',	'/tenant-dokumentasi/index'),
('user',	'/tenant-dokumentasi/index'),
('admin',	'/tenant-dokumentasi/update'),
('user',	'/tenant-dokumentasi/update'),
('admin',	'/tenant-dokumentasi/view'),
('user',	'/tenant-dokumentasi/view'),
('admin',	'/tenant-kegiatan/*'),
('user',	'/tenant-kegiatan/*'),
('admin',	'/tenant-kegiatan/create'),
('user',	'/tenant-kegiatan/create'),
('admin',	'/tenant-kegiatan/delete'),
('user',	'/tenant-kegiatan/delete'),
('admin',	'/tenant-kegiatan/index'),
('user',	'/tenant-kegiatan/index'),
('admin',	'/tenant-kegiatan/update'),
('user',	'/tenant-kegiatan/update'),
('admin',	'/tenant-kegiatan/view'),
('user',	'/tenant-kegiatan/view'),
('admin',	'/tenant-logbook/*'),
('user',	'/tenant-logbook/*'),
('admin',	'/tenant-logbook/create'),
('user',	'/tenant-logbook/create'),
('admin',	'/tenant-logbook/delete'),
('user',	'/tenant-logbook/delete'),
('admin',	'/tenant-logbook/index'),
('user',	'/tenant-logbook/index'),
('admin',	'/tenant-logbook/logbook'),
('admin',	'/tenant-logbook/update'),
('user',	'/tenant-logbook/update'),
('admin',	'/tenant-logbook/view'),
('user',	'/tenant-logbook/view'),
('admin',	'/tenant-milestone/*'),
('user',	'/tenant-milestone/*'),
('admin',	'/tenant-milestone/create'),
('user',	'/tenant-milestone/create'),
('admin',	'/tenant-milestone/delete'),
('user',	'/tenant-milestone/delete'),
('admin',	'/tenant-milestone/index'),
('user',	'/tenant-milestone/index'),
('admin',	'/tenant-milestone/milestone'),
('admin',	'/tenant-milestone/update'),
('user',	'/tenant-milestone/update'),
('admin',	'/tenant-milestone/view'),
('user',	'/tenant-milestone/view'),
('admin',	'/tenant-rab/*'),
('user',	'/tenant-rab/*'),
('admin',	'/tenant-rab/create'),
('user',	'/tenant-rab/create'),
('admin',	'/tenant-rab/delete'),
('user',	'/tenant-rab/delete'),
('admin',	'/tenant-rab/index'),
('user',	'/tenant-rab/index'),
('admin',	'/tenant-rab/rab'),
('admin',	'/tenant-rab/update'),
('user',	'/tenant-rab/update'),
('admin',	'/tenant-rab/view'),
('user',	'/tenant-rab/view'),
('admin',	'backend_pic_stp_all'),
('admin',	'backend_tenant_kegiatan_all'),
('admin',	'Left Menu Admin'),
('admin',	'Left Menu Berita'),
('admin',	'Left Menu Fasilitas'),
('admin',	'Left Menu Galery'),
('admin',	'Left Menu Kode'),
('admin',	'Left Menu RBAC'),
('admin',	'Left Menu Service');

DROP TABLE IF EXISTS `auth_rule`;
CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

TRUNCATE `auth_rule`;

DROP TABLE IF EXISTS `berita`;
CREATE TABLE `berita` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul_berita` varchar(200) NOT NULL,
  `sumber_berita` varchar(200) NOT NULL,
  `isi_berita` text NOT NULL,
  `tanggal_publikasi` datetime NOT NULL,
  `upload_foto` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE `berita`;
INSERT INTO `berita` (`id`, `judul_berita`, `sumber_berita`, `isi_berita`, `tanggal_publikasi`, `upload_foto`) VALUES
(4,	'Science and Techno Park',	'Pusat Inovasi LIPI',	'<p>Science Techno Park (STP) adalah merupakan kawasan yang dikelola secara profesional dengan tujuan meningkatkan kesejahteraan/perekonomian masyarakat sekitarnya dengan mempromosikan budaya inovasi dan daya saing dari bisnis berbasis pengetahuan.</p>\r\n\r\n<p><img alt=\"\" src=\"https://cdn.pbrd.co/images/Hk3fxeS.jpg\" style=\"height:273px; width:300px\" /></p>\r\n\r\n<p>STP merupakan penyedia pengetahuan terkini, penyedia solusi teknologi, serta merupakan pusat pengembangan aplikasi teknologi lanjut.</p>\r\n\r\n<p>Prinsip dasar STP</p>\r\n\r\n<ol>\r\n	<li>Adanya jaminan keberlanjutan</li>\r\n	<li>Penerapan teknologi sesuai kebutuhan</li>\r\n	<li>Ramah Lingkungan</li>\r\n	<li>Menciptakan lapangan Kerja</li>\r\n	<li>Terintegrasi</li>\r\n</ol>\r\n\r\n<p>Tujuan STP</p>\r\n\r\n<ol>\r\n	<li>Mendorong penciptaan perusahaan-perusahaan start up dan perusahaan baru;</li>\r\n	<li>Mencapai alih pengetahuan dan teknologi dari perguruan tinggi dan lembaga litbang ke entitas bisnis;</li>\r\n	<li>Penciptaan lapangan pekerjaan; dan</li>\r\n	<li>Menarik teknologi tinggi</li>\r\n</ol>\r\n\r\n<p>Kegiatan STP</p>\r\n\r\n<ol>\r\n	<li>Riset dan networking</li>\r\n	<li>Kerjasama komersial</li>\r\n	<li>Pendidikan dan pelatihan (technopreneurship)</li>\r\n	<li>Difusi inovasi teknologi</li>\r\n	<li>alih teknologi</li>\r\n	<li>Intermediasi (inkubasi teknologi, alih teknologi, manajemen HKI)</li>\r\n	<li>fasilitasi akses (pasar, pembiayaan/pendanaa, SDM, teknologi, investor)</li>\r\n	<li>Penyediaan fasilitas</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n',	'2015-05-02 22:25:51',	'home.jpg'),
(5,	'SIDOK STP LIPI',	'Pusat Inovasi LIPI',	'<p>Dalam rangka meningkatkan akses publik terhadap kegiatan Science Technology Park yang dilaksanakan di LIPI, telah diluncurkan sistem Informasi dan Dokumentasi Kegiatan Science &amp; Technology Park di LIPI. Sistem ini akan mewadahi informasi dan dokumentasi kegiatan yang dilaksanakan di STP LIPI.</p>\r\n\r\n<p>Dalam situs ini, pengunjung bisa melihat kegiatan-kegiatan yang diwadahi dalam STP LIPI yang diunggah oleh masing-masing pelaksana kegiatan STP LIPI. Dengan demikian, infomasi dan komunikasi bisa dimanfaatkan oleh pengunjung.</p>\r\n\r\n<p>Untuk mengakses situs tersebut, silahkan mengunjungi&nbsp;<a href=\"http://inovasi.lipi.go.id/sidok\">http://inovasi.lipi.go.id/sidok</a></p>\r\n',	'2015-10-02 03:30:57',	'screenshot.jpg');

DROP TABLE IF EXISTS `daftar_tenant`;
CREATE TABLE `daftar_tenant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_tenant` varchar(200) NOT NULL,
  `email_tenant` varchar(300) NOT NULL,
  `alamat` text NOT NULL,
  `no_hp` varchar(45) NOT NULL,
  `logo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE `daftar_tenant`;
INSERT INTO `daftar_tenant` (`id`, `nama_tenant`, `email_tenant`, `alamat`, `no_hp`, `logo`) VALUES
(3,	'PT NANOTECH INOVASI INDONESIA',	'nanotech@nano.or.id',	'INKUBATOR TEKNOLOGI LIPI KABUPATEN BOGOR CIBINONG PROVINSI JAWA BARAT',	'+62 8128179****',	'nanovasi.jpg'),
(4,	'CV MARANTI',	'nn@nn',	'INKUBATOR TEKNOLOGI LIPI KABUPATEN BOGOR CIBINONG PROVINSI JAWA BARAT',	'+62 1234567****',	'maranti v-01.jpg'),
(5,	'PT DNR INTERNATIONAL',	'nandahendra135@gmail.com',	'INKUBATOR TEKNOLOGI LIPI KOTA JAKARTA SELATAN JAKARTA PROVINSI DKI JAKARTA',	'+62 8123461****',	'DNR International.jpg'),
(6,	'CV RUMAH USAHA BERSAMA',	'rumahusahabersama@gmail.com',	'B6 INKUBATOR TEKNOLOGI LIPI KABUPATEN BOGOR NANGGEWER PROVINSI JAWA BARAT',	' +62 8128228****',	'Rumah Usaha Bersama.jpg'),
(7,	'PT AWINA SINERGI INDONESIA',	'siti.greenland@gmail.com',	'INKUBATOR TEKNOLOGI LIPI KABUPATEN BOGOR NANGGEWER PROVINSI JAWA BARAT',	'+62 8158454****',	'Awina.jpg'),
(8,	'PT TRITUNGGAL PRAKARSA GLOBAL',	'hakim.pane@gmail.com',	'INKUBATOR TEKNOLOGI LIPI KOTA JAKARTA SELATAN TEBET PROVINSI DKI JAKARTA',	'+62 8211105****',	'tritunggal prakarsa.jpg'),
(12,	'PT PANEN ENERGI INDONESIA',	'nn@nn',	' INKUBATOR TEKNOLOGI LIPI KABUPATEN BOGOR CIBINONG PROVINSI JAWA BARAT ',	'+62 123456789__',	'panen energi.jpg'),
(13,	'CV MYCO PANGAN INDONESIA',	'jm.hanggoro@gmail.com',	'B7 INKUBATOR TEKNOLOGI LIPI KABUPATEN BOGOR CIBINONG PROVINSI JAWA BARAT',	'+62 87770464809',	'myco pangan.jpg'),
(14,	'PT BOLMONG CANTIKA JAYA',	'nn@nn',	'INKUBATOR TEKNOLOGI LIPI KABUPATEN BOGOR CIBINONG PROVINSI JAWA BARAT',	'+62 8119899961_',	'Bolmong cantik jaya.jpg'),
(15,	'PT LABO CHEMI INDONESIA',	'eka.julia@sci.ui.ac.id',	'INKUBATOR TEKNOLOGI LIPI KABUPATEN BOGOR NANGGEWER PROVINSI JAWA BARAT',	'+62 85742979111',	'Labo chemi.jpg'),
(16,	'CV FISIKA LABORATORIA',	'global.fisika.laboratoria@gmail.com',	'KOMPLEK BATAN INDAH, KADEMANGAN KOTA TANGERANG SELATAN SERPONG PROVINSI BANTEN',	'+62 82284161116',	'Fisika Laboratoria-01.jpg'),
(17,	'PT HIGH QUALITY CORPORA PUTRA',	'didi.diarsa@gmail.com',	'INKUBATOR TEKNOLOGI LIPI KOTA TANGERANG TANGGERANG PROVINSI BANTEN',	'+62 818252614__',	'HQCP-01.jpg'),
(18,	'CV MEDIA SARANA USAHA',	' mediasaranausaha@gmail.com',	' INKUBATOR TEKNOLOGI LIPI KABUPATEN BOGOR CIBINONG PROVINSI JAWA BARAT',	'+62 82314464363',	'CV Media sarana Usaha.jpg');

DROP TABLE IF EXISTS `fasilitas`;
CREATE TABLE `fasilitas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_fasilitas` varchar(200) NOT NULL,
  `isi_fasilitas` text NOT NULL,
  `upload_gambar` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE `fasilitas`;
INSERT INTO `fasilitas` (`id`, `nama_fasilitas`, `isi_fasilitas`, `upload_gambar`) VALUES
(3,	'Pilot Plant (3 Buildings)',	'<ol>\r\n	<li>Biocatalyst Product</li>\r\n	<li>Traditional Medicine</li>\r\n	<li>Medical Devices &amp; Diagnostic Kit</li>\r\n</ol>\r\n',	'pilot_plan.jpg'),
(4,	'Tenant Building',	'<p>Number of Buildings&nbsp; &nbsp; &nbsp; &nbsp;: 5 Buildings</p>\r\n\r\n<p>Number of Office Space : 50 room/building</p>\r\n\r\n<p>Office Space Area&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : 40 - 60 sqm</p>\r\n',	'tenant_building.jpg'),
(5,	'Co-working  Space & Business Center',	'<p>The coworking space and business center in C-STP LIPI provides a comprehensive working space and business center such as: print center, office shop, even cafe to improve the working experience.</p>\r\n',	'cws.jpg'),
(6,	'Innovation Convention & Exhibition Center',	'<p>With the area up to 8900 sqm, this place can hold conference, exhibition, seminar, of another forum just a step from the office space and pilot plan facilities.</p>\r\n',	'convertion.jpg'),
(7,	'Innovation Guest House',	'<p>C-STP LIPI also provide guest house with the 3-star International hotel standard</p>\r\n',	'cws.jpg');

DROP TABLE IF EXISTS `fungsi_stp`;
CREATE TABLE `fungsi_stp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fungsi` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE `fungsi_stp`;
INSERT INTO `fungsi_stp` (`id`, `fungsi`) VALUES
(1,	'Center for Advanced Technology Development'),
(2,	'Center for Knowledge-based Industry Development'),
(3,	'Center for Technopreneurship'),
(4,	'Facilitation of Intellectual Property Management'),
(5,	'Technology Incubation'),
(6,	'Technology Transfer and Commercialization'),
(7,	'Provision of Scientific and Technological Services to SMES'),
(8,	'Technological Innovation Hub for Regional Innovation System');

DROP TABLE IF EXISTS `galery_stp`;
CREATE TABLE `galery_stp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `foto` varchar(100) NOT NULL,
  `nama` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE `galery_stp`;
INSERT INTO `galery_stp` (`id`, `foto`, `nama`) VALUES
(2,	'img_8250.jpg',	'Lokakarya Nasional Science And Technology Park (STP) 2015 STP, 2015'),
(3,	'drafting_paten_pusat_inovasi_lipi_1.jpg',	'Pelatihan Drafting Paten Tingkat Lanjut 2014'),
(4,	'img_6750.jpg',	'Pekan Inovasi & Teknologi 2014 2014'),
(5,	'1.jpg',	'Pelatihan Drafting Paten untuk Umum 2014');

DROP TABLE IF EXISTS `kegiatan`;
CREATE TABLE `kegiatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul_kegiatan` varchar(200) NOT NULL,
  `isi_kegiatan` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE `kegiatan`;
INSERT INTO `kegiatan` (`id`, `judul_kegiatan`, `isi_kegiatan`) VALUES
(3,	'Kegiatan STP',	'<p>Kegiatan STP LIPI yang dilaksanakan antara lain:</p>\r\n\r\n<ol>\r\n	<li>Pengembangan Perusahaan Start-Up Berbasis Inovasi Teknologi Tenan STP LIPI dan pengelolaannya</li>\r\n	<li>Pegembangan 10 produk berbasis inovasi teknologi industri di STP (teknologi bisa dari industri atau lemlit) dan pengelolaannya.</li>\r\n	<li>Pengelolaan Intermediasi Alih Teknologi dan Promosi Jasa STP LIPI</li>\r\n	<li>Kajian dan Penyusunan Kebijakan Insentif industri tenan STP dan praktek pengelolaan STP</li>\r\n	<li>Penguatan kapasitas SDM di bidang technoprenership (100 Technopreners)</li>\r\n	<li>Validasi teknis dan ekonomis teknologi bidang hayati (118 teknologi, 10 bidang hayati)</li>\r\n	<li>Penyediaan Fasilitas Manufaktur Pilot Plant Untuk Akselerasi Alih teknologi Bidang Hayati di STP LIPI</li>\r\n</ol>\r\n');

DROP TABLE IF EXISTS `kode_anggaran`;
CREATE TABLE `kode_anggaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` int(11) NOT NULL,
  `ket_anggaran` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE `kode_anggaran`;
INSERT INTO `kode_anggaran` (`id`, `kode`, `ket_anggaran`) VALUES
(1,	521211,	'Belanja Bahan'),
(2,	521213,	'Honor Output Kegiatan'),
(3,	521219,	'Belanja Barang Non Operasional Lainnya'),
(4,	521811,	'Belanja Barang Persediaan Barang Konsumsi'),
(5,	524111,	'Belanja Perjalanan Biasa'),
(6,	121212121,	'A maiores aliquam praesentium earum in consequatur laborum');

DROP TABLE IF EXISTS `konsep_stp`;
CREATE TABLE `konsep_stp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isi_konsep` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE `konsep_stp`;
INSERT INTO `konsep_stp` (`id`, `isi_konsep`) VALUES
(1,	'<p>Pengembangan Science and Techno Park (STP), Science Park (SP), dan Techno Park (TP) merupakan salah satu program nasional&mdash;butir ke-6 Nawacita Pemerintahan Presiden Joko Widodo. Tujuan Program ini adalah untuk meningkatkan produktifitas masyarakat dan daya saing industri di pasar global. Pada tahun 2015 Pemerintah telah menetapkan 100 STP dan TP yang dikelola oleh berbagai instansi Pemerintah dan juga sektor swasta.</p>\r\n\r\n<p>Arah Kebijakan Pembangunan Science Park dan Techno Park telah ditetapkan dalam Rencana Pembangunan Jangka Menengah Nasional (RPJMN) 2015-2019, Perpres No. 2 Tahun 2015&nbsp;</p>\r\n');

DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `route` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `data` blob,
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`),
  CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `menu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

TRUNCATE `menu`;
INSERT INTO `menu` (`id`, `name`, `parent`, `route`, `order`, `data`) VALUES
(1,	'backend_left_menu',	NULL,	NULL,	NULL,	NULL),
(2,	'1',	NULL,	NULL,	NULL,	NULL),
(3,	'1',	NULL,	NULL,	NULL,	NULL);

DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE `migration`;
INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base',	1523437120),
('m130524_201442_init',	1523437171),
('m140506_102106_rbac_init',	1523527766),
('m140602_111327_create_menu_table',	1523527749),
('m160312_050000_create_user',	1523527749),
('m170907_052038_rbac_add_index_on_auth_assignment_user_id',	1523527767);

DROP TABLE IF EXISTS `pic_stp`;
CREATE TABLE `pic_stp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pic` varchar(200) NOT NULL,
  `jabatan` varchar(200) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `nip` varchar(200) NOT NULL,
  `jenis_kelamin` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE `pic_stp`;
INSERT INTO `pic_stp` (`id`, `nama_pic`, `jabatan`, `tgl_lahir`, `nip`, `jenis_kelamin`, `email`, `id_user`) VALUES
(2,	'Desi Ratnasari',	'AM1',	'2018-04-13',	'0112104007',	'P',	'desiratnasari866@gmail.com',	1),
(3,	'Gita Andini',	'AM2',	'2018-04-07',	'011012201201',	'P',	'gitaandini@gmail.com',	3),
(6,	'Drs. Manaek Simamora, MBA',	'Koordinator Utama C-STP',	'2018-04-08',	'196211111990031002',	'L',	'manaek@yahoo.com',	4),
(7,	'Nurlisa Dwi Novianti, S. Farm., Apt.',	'Koordinator Teknis Kegiatan C-STP',	'2018-05-01',	'198911012014012001',	'P',	'Nurlisa.dwi@gmail.com',	5),
(8,	'Aris Yaman, S.Stat',	'PIC Techno Park LIPI',	'2018-05-13',	' 198708072014011001',	'L',	'Aris.yaman@gmail.com',	6),
(9,	'Tsilmi Surraya, S.E',	'PIC C-STP',	'2018-04-17',	' 198708072014022222',	'P',	'Silmitsurayya12@gmail.com',	7),
(10,	'C. Tony Prasetyo, S.E',	'PIC C-STP',	'2018-03-11',	' 198708072014033333',	'L',	'Cornelius.tony.p@gmail.com',	8),
(11,	'Quinntita Rofifadhila, STP',	'PIC C-STP',	'2018-04-14',	' 198708072014055555',	'P',	'rquinntita@gmail.com',	9),
(12,	'Nurlaily Dwi kartiana, S.P',	'PIC C-STP',	'2018-04-18',	' 198708072014066666',	'P',	'nurlailydk@gmail.com',	10);

DROP TABLE IF EXISTS `profile_stp`;
CREATE TABLE `profile_stp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `latar_belakang` text NOT NULL,
  `visi` text NOT NULL,
  `misi` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE `profile_stp`;
INSERT INTO `profile_stp` (`id`, `latar_belakang`, `visi`, `misi`) VALUES
(1,	'<p>Cibinong Science and Technology Park &ndash; Lembaga Ilmu Pengetahuan Indonesia (CSTP-LIPI) is one of a number of National-STP&rsquo;s being developed by the Government of Indonesia since 2015. Currently, C-STP LIPI is managed by Center for Innovation LIPI. C-STP LIPI is also one of National Strategic Projects of the Republic of Indonesia since 2016 and it is directly under the supervision of the President&rsquo;s Office. C-STP LIPI is in its early stage of development. Its development can be traced back to the establishment of Center for Innovation LIPI in 2001, initialy located in LIPI Headquarters, Jakarta. Center for Innovation itself was a transformation from Technology Service Office- Management System Strengthening LIPI with the main mission to transform LIPI into a professional service provider to industry.<br />\r\n<img alt=\"\" src=\"https://cdn.pbrd.co/images/HlnernB.jpg\" style=\"float:left; height:300px; margin:20px; width:480px\" /></p>\r\n\r\n<p>C-STP LIPI main objectives are to accelerate utilization of research results and nurture the growth of technology-based business and enhance people productivity and industrial competitiveness. To achieve its objectives, C-STP LIPI provide services on Technology and Innovation Development, Technology Commercialization, Facility, and Business Services.</p>\r\n\r\n<p>Since its inception in 2015, C-STP LIPI has directly facilitated the creation of 8 startups, provide services to 20 tenants, train more than 200 people in the area of technology adoption, and created 5 technology licences. It also has facilitated the development of more than 20 high prospective technology and now are in its pipeline to be commercialized. C-STP LIPI also attracts technology from overseas to be promoted.<br />\r\n&nbsp;</p>\r\n',	'<p>Delivering excellent innovation services for acceleration of technology commercialization and growth of competitive knowledge-based enterprise in enhancing economic development.</p>\r\n',	'<ul>\r\n	<li>To facilitate acceleration of development, utilization, and commercialization of technology into startup and industrial application in the areas of health and medicine, medical devices, food, cosmetics, green energy, agriculture and ICT.</li>\r\n	<li>To provide technological services to enhance SMEs and industrial competitiveness.</li>\r\n	<li>To nurture talents to accelerate science and technology commercialization and technology-based enterprise development.</li>\r\n	<li>To promote global innovation network in strengthening collaboration in the technological development, technology transfer, and hi-tech enterprise development.</li>\r\n</ul>\r\n');

DROP TABLE IF EXISTS `service`;
CREATE TABLE `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_service` text NOT NULL,
  `detail_service` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE `service`;
INSERT INTO `service` (`id`, `nama_service`, `detail_service`) VALUES
(1,	'Technology and Innovation Services',	'<ul>\r\n	<li>Intellectual Property Management Consultancy</li>\r\n	<li>New Product Development</li>\r\n	<li>Facilitation and Provision oof Technology &amp; Innovation Fund</li>\r\n	<li>Direct Access to Leading Research Center</li>\r\n</ul>\r\n'),
(2,	'Business Services',	'<ul>\r\n	<li>Acess to VC</li>\r\n	<li>Facilitation Access to Seed Capitals and Investment</li>\r\n	<li>Business Development</li>\r\n	<li>Business Planning</li>\r\n	<li>Networking</li>\r\n</ul>\r\n'),
(3,	'Technology Commercialization',	'<ul>\r\n	<li>Technology Valuation</li>\r\n	<li>Business Matching</li>\r\n	<li>Business Pitching</li>\r\n	<li>Technology Forum and Exhibition</li>\r\n</ul>\r\n'),
(4,	'Facility Services',	'<ul>\r\n	<li>Pilot Plan Facilities</li>\r\n	<li>Office Space</li>\r\n	<li>Conference/Exhibition/Meeting Rooms</li>\r\n	<li>Cafetaria</li>\r\n	<li>Guest House</li>\r\n</ul>\r\n');

DROP TABLE IF EXISTS `tenant_catatan`;
CREATE TABLE `tenant_catatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kegiatan` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `lokasi` varchar(300) NOT NULL,
  `catatan_pertemuan` text NOT NULL,
  `rencana` text NOT NULL,
  `keterangan` varchar(300) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_kegiatan` (`id_kegiatan`),
  CONSTRAINT `tenant_catatan_ibfk_1` FOREIGN KEY (`id_kegiatan`) REFERENCES `tenant_kegiatan` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE `tenant_catatan`;
INSERT INTO `tenant_catatan` (`id`, `id_kegiatan`, `tanggal`, `lokasi`, `catatan_pertemuan`, `rencana`, `keterangan`) VALUES
(1,	6,	'2018-05-01',	'Ruang Departemen Onkologi Ginekologi RSCM',	'Pengadaan Barang',	'Implementasi',	'Tidak Ada'),
(2,	6,	'2018-05-10',	'Pusat Inovasi LIPI',	'Tahap Pengadaan ke-2',	'Pengaplikasian',	'Tidak Ada');

DROP TABLE IF EXISTS `tenant_dokumentasi`;
CREATE TABLE `tenant_dokumentasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kegiatan` int(11) NOT NULL,
  `nama_kegiatan` varchar(300) NOT NULL,
  `upload_foto` varchar(300) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_kegiatan` (`id_kegiatan`),
  CONSTRAINT `tenant_dokumentasi_ibfk_1` FOREIGN KEY (`id_kegiatan`) REFERENCES `tenant_kegiatan` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE `tenant_dokumentasi`;
INSERT INTO `tenant_dokumentasi` (`id`, `id_kegiatan`, `nama_kegiatan`, `upload_foto`) VALUES
(1,	6,	'Tanda Tangan PKS',	'cahaya.PNG'),
(3,	6,	'Tes',	'img_8250.jpg');

DROP TABLE IF EXISTS `tenant_kegiatan`;
CREATE TABLE `tenant_kegiatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `nama_kegiatan` varchar(300) NOT NULL,
  `koordinator_peneliti` varchar(300) NOT NULL,
  `judul` varchar(300) NOT NULL,
  `tujuan` text NOT NULL,
  `sasaran` text NOT NULL,
  `id_pic` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_pic` (`id_pic`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `tenant_kegiatan_ibfk_1` FOREIGN KEY (`id_pic`) REFERENCES `pic_stp` (`id`),
  CONSTRAINT `tenant_kegiatan_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE `tenant_kegiatan`;
INSERT INTO `tenant_kegiatan` (`id`, `id_user`, `nama_kegiatan`, `koordinator_peneliti`, `judul`, `tujuan`, `sasaran`, `id_pic`) VALUES
(5,	3,	'Tas',	'Yoga',	'Tas Sekolah',	'<p>Sekolah Dasar</p>\r\n',	'<p>Siswa</p>\r\n',	3),
(6,	1,	'Fasilitas Penciptaan danPendampingan Tenant C-STP Teknologi Diagnostik Kit Kanker Serviks',	'Dr. Sukma Nuswantara, M.Phil',	'Fasilitas Penciptaan danPendampingan Tenant C-STP Teknologi Diagnostik Kit Kanker Serviks',	'<p>Validasi teknis komersial diagnostik kit kanker serviks untuk memenuhi kebutuhan rapid test kanker dalam negeri.</p>\r\n',	'<ol>\r\n	<li>Hasil uji produk baru (Kit Cervix-5) teruji stabilitas, spesifitas, sentivitas dengan sampel urin (non-invasive) dan sampel darah segar pasien</li>\r\n	<li>PKS Pemanfaatan produk ( dengan HOGI/RSCM)</li>\r\n	<li>PKS Lisensi (dengan PT Phapros)</li>\r\n</ol>\r\n',	3);

DROP TABLE IF EXISTS `tenant_logbook`;
CREATE TABLE `tenant_logbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kegiatan` int(11) NOT NULL,
  `jenis_belanja` varchar(300) NOT NULL,
  `nominal` double NOT NULL,
  `tanggal_pengajuan` date NOT NULL,
  `tanggal_pencairan` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_kegiatan` (`id_kegiatan`),
  CONSTRAINT `tenant_logbook_ibfk_1` FOREIGN KEY (`id_kegiatan`) REFERENCES `tenant_kegiatan` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE `tenant_logbook`;
INSERT INTO `tenant_logbook` (`id`, `id_kegiatan`, `jenis_belanja`, `nominal`, `tanggal_pengajuan`, `tanggal_pencairan`) VALUES
(1,	6,	'Perjalanan Ke RSCM Kamis, 22/02/18 Koordinasi dengan HOGI ttg Uji Klinis',	900000,	'2018-05-07',	'2018-05-25'),
(2,	6,	'Pengadaan Bahan',	150000000,	'2018-05-18',	'2018-05-21');

DROP TABLE IF EXISTS `tenant_milestone`;
CREATE TABLE `tenant_milestone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kegiatan` int(11) NOT NULL,
  `bulan` date NOT NULL,
  `keterangan` varchar(300) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_kegiatan` (`id_kegiatan`),
  CONSTRAINT `tenant_milestone_ibfk_1` FOREIGN KEY (`id_kegiatan`) REFERENCES `tenant_kegiatan` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE `tenant_milestone`;
INSERT INTO `tenant_milestone` (`id`, `id_kegiatan`, `bulan`, `keterangan`) VALUES
(5,	6,	'2018-06-01',	'<p>Tes</p>\r\n'),
(6,	6,	'2018-03-01',	'<p>Buku Tulis 2</p>\r\n'),
(7,	6,	'2018-07-01',	'<p>hariiii</p>\r\n');

DROP TABLE IF EXISTS `tenant_rab`;
CREATE TABLE `tenant_rab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kegiatan` int(11) NOT NULL,
  `jumlah` double NOT NULL,
  `upload_rab` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_kegiatan` (`id_kegiatan`),
  CONSTRAINT `tenant_rab_ibfk_1` FOREIGN KEY (`id_kegiatan`) REFERENCES `tenant_kegiatan` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE `tenant_rab`;
INSERT INTO `tenant_rab` (`id`, `id_kegiatan`, `jumlah`, `upload_rab`) VALUES
(3,	6,	150000000,	'RabKit.png'),
(4,	5,	200000,	'RabKit.png');

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

TRUNCATE `user`;
INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1,	'admin',	'Z7IIZp24U4gYAP72hM66-bYqpWIQnKoL',	'$2y$13$Yc8HOvrkqTT0m.OQGBWsA.G4wbdGcoVbzSCkg9URLhr8KnRxV9sP.',	NULL,	'desiratnasari866@gmail.com',	10,	1523511729,	1523511729),
(2,	'desi',	'VOm3r8at1_QW35I7cpI1WDva2RhEX13f',	'$2y$13$L3XoXPEYWEFk58w91aiexuvS8Sdq9Ehp7IWmV8Tx8iPmKnI6KDzYq',	NULL,	'desiratnasari541@gmail.com',	10,	1523526724,	1523526724),
(3,	'gita',	'GChae5SnY3KANwZ56-3nzB3LBkSQODrF',	'$2y$13$P85../xCvS3q//cFU7RyhOMYgooF0Mmjw69y.DXS07ZrInF4ftjd.',	NULL,	'gitaandini@gmail.com',	10,	1524149452,	1524149452),
(4,	'manaek',	'ZBUIlD1lRt9RwX3h7WSpO6StU-5o59zL',	'$2y$13$nKUdXkYdwDA4xJJxgiBW3.Gawaz2yid5ZniNxiFFpaEHfLl4EEt7W',	NULL,	'manaek@yahoo.com',	10,	1526956678,	1526956678),
(5,	'nurlisa',	'-T5-00j53NIgryFhvVBPZ7GuLcCMTs7M',	'$2y$13$nwoihbi0saxMQWro03rJy.Fp1U/hgXVTTrhVZR7ok4SkE0GkqWc3e',	NULL,	'nurlisa.dwi@gmail.com',	10,	1526956773,	1526956773),
(6,	'arisyaman',	'Y-UgIT8ruQ25SEIZuBQXKBLtzSyEWnf0',	'$2y$13$Nv82bP27z0pSbv989jMN5.liL5.kilSMIdHRE4CZwpYl4WfhwCKfm',	NULL,	'aris.yaman@gmail.com',	10,	1526956950,	1526956950),
(7,	'tsilmi',	'gVfv1GD4BfzpSc-xfL-UmwCxviwnpJ30',	'$2y$13$uRfhyd7UVL7MQiNQBGT/neVM/qGOFr6Wrm9.g1OLG9MMx7Q41zoC6',	NULL,	'silmitsurayya12@gmail.com',	10,	1526957010,	1526957010),
(8,	'tony',	'v1mhE4x6ky2vlxCKZvYoi55wwT5Li9sj',	'$2y$13$Lahd9FINwv2zxylDTc2ELOrv1yaP9SZu.3s6o8nBqYZUi6gdMf9Re',	NULL,	'cornelius.tony.p@gmail.com',	10,	1526957092,	1526957092),
(9,	'quinntita',	'VBwlveGjanDkwAyH4z8BNP5ee8hmSS_J',	'$2y$13$fANoMYVfXausIctJSlYhOehSKGKNNJkRND/VQpncAOTxqL04H4E9G',	NULL,	'rquinntita@gmail.com',	10,	1526957158,	1526957158),
(10,	'nurlaily',	'-Xy-rlVqirr6H3EFUsc7UefJrbyxSkHZ',	'$2y$13$KWG4Plzflhdm3wcHjvMpeuJfCuv1u.MINUqFmB27V67jd7UazHVBS',	NULL,	'nurlailydk@gmail.com',	10,	1526957212,	1526957212);

-- 2018-08-17 09:47:03
